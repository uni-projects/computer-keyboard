;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.

(defun gen-maps ()
  (with-open-file 
   (keymap "keymap.lisp" :direction :output :if-exists :supersede)
   (format keymap "(defkey :shift 1 1)~%")
   (with-open-file 
    (charmap "charmap.lisp" :direction :output :if-exists :supersede)
    (do ((x (char-code #\a) (1+ x)))
	((= x (1+ (char-code #\z))) T)
      (format keymap "(defkey :~a 1 1)~%" (code-char x))
      (format charmap "(compose #\\~a (:~a :f1))~%" (code-char x) (code-char x))
      (format charmap "(compose #\\~a (:shift :f1) (:~a :f2))~%" (char-upcase (code-char x)) (code-char x))))))
  ;; (do ((x (char-code #\A) (1+ x)))
  ;;     ((= x (1+ (char-code #\Z))) T)
  ;;   (format t "(defkey :~a 1 1)~%" (code-char x))
  ;;   (format t "(compose #\~a (:~a :f1))~%" (code-char x) (code-char x)))

  ;;   (format t "~a ~a~%" x (code-char x))))

(defun save-map (path map)
  (with-open-file
      (keymap path :direction :output :if-exists :supersede)
    (format keymap "~{~S~%~}" map)))

(defparameter *qwerty*
  '((nil nil nil nil :space nil nil nil :alt)
    (:shift :z :x :c :v :b :n :m :comma :dot)
    (nil :a :s :d :f :g :h :j :k :l :semi :apostrphe)
    (nil :q :w :e :r :t :y :u :i :o :p :left-bracket :right-bracket)))

(defparameter fingers nil)
(defun deffinger (alias x y)
  (push (list alias (complex x y)) fingers))

(defparameter keys nil)
(defun defkey (alias x y)
  (push (list alias (complex x y)) keys))

(defun best-fit (symbol &optional blacklist)
  (format t "black ~a~&" blacklist)
  (let (best
	(k (find symbol keys :key #'car)))
    (dolist (f 
	      (remove-if (lambda (x) (find x blacklist)) fingers :key #'car)
	     best)
      (format t "~a~%" f)
      (if (not best)
	  (setf best f)
	  (when (<
		 (abs (- (cadr f)
			 (cadr k)))
		 (abs (- (cadr best)
			 (cadr k))))
	    (setf best f))))))

(defun assemble-charmap (chars)
  (mapcar (lambda (char)
	    (apply #'list 'compose char))
	  chars))

(defun assemble-keymap (keys coords)
  (mapcar (lambda (args)
	    (apply #'list 'defkey args))
	  (mapcar #'cons
		  keys
		  coords)))

(defun gen-charmap (layout)
  (let (result)
    (assemble-charmap
     (dolist (symbol (reduce #'append layout) result)
       (when (and symbol
		  (eql 1
		       (length (symbol-name symbol))))
	 (progn
	   (push
	    (list
	     (elt 
	      (string-downcase 
	       (symbol-name symbol))
	      0)
	     (list symbol 
		   (car (best-fit symbol))))
	    result)
	   (push
	    (list
	     (elt
	      (symbol-name symbol)
	      0)
	     (list :shift
		   (car (best-fit :shift)))
	     (list symbol 
		   (car (best-fit symbol '(:lewy-maly)))))
	    result)))))))


(defun gen-keymap (layout)
  (let (result)
    (dolist (row layout (apply #'append result))
      (push
       (remove nil
	       (assemble-keymap
		row
		(gen-coords 1
			    (1+ (position row layout))
			    (length row)
			    (1+ (position row layout))))
	       :key #'cadr)
       result))))

(defun gen-coords (x-min y-min x-max y-max)
  (labels ((gen-coords-rec (x y)
	     (cons (list x y)
		   (cond
		     ((and (= x x-max)
			   (= y y-max)) nil)
		     ((= x x-max)
		      (gen-coords-rec 1 (1+ y)))
		     (T
		      (gen-coords-rec (1+ x) y))))))
    (gen-coords-rec x-min y-min)))
