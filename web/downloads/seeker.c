#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

FILE *file;
int ktoryZnak=0;
int agr=0;
int max=0;

int main(int argc, char** argv)
{
	if(argc<2)
	{
		printf("Podaj jako argument sciezke do pliku tekstowego.\n");
		return -1;
	}
	
	if ((file=fopen(argv[1], "r"))==NULL) 
	{
		printf("Otwarcie pliku zakonczylo sie niepowodzeniem.\n");
		return -1;
	}
	
	printf("Podaj numer wystąpienia znaku w korpusie: ");
	scanf("%d",&ktoryZnak);
	ktoryZnak++;
	printf("Podaj szerokosc przedzialu agregowania: ");
	scanf("%d",&agr);
	
	int i=0;
	while(1)
	{
		if(fgetwc(file)==WEOF) break;
		max++;			
	}
	
	if(ktoryZnak<0 || ktoryZnak>max)
	{
		printf("Niepoprawne parametry wejsciowe.\n");
		return -1;
	}
	
	wchar_t* buffer = (wchar_t*)malloc((max+1)*sizeof(wchar_t));
	fseek(file, 0, 0);
	fwscanf(file, L"%s", &buffer[0]);
	fclose (file);
	
	if(agr>=max)
	{
		for(i=0; i<ktoryZnak; i++)
		{
			printf("%lc",buffer[i]);
		}
	}
	else if((ktoryZnak-agr)<0)
	{
		for(i=0; i<ktoryZnak; i++)
		{
			printf("%lc",buffer[i]);
		}
	}
	else
	{
		for(i=(ktoryZnak-agr); i<ktoryZnak; i++)
		{
			printf("%lc",buffer[i]);
		}
	}
	
	free(buffer);
	return 0;

}
