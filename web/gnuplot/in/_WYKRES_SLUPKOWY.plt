#wywolanie z konsoli: sciezka_do_gnuplot.exe "sciezka_do_skryptu"
# gnuplot.exe "web/gnuplot/in/_WYKRES_SLUPKOWY.dem"

reset
set encoding utf8
#wyswietlenie w gnuplocie
#set term win size 1000,600

#utworzenie canvasa w js
set term canvas size 1000,600 name 'fingers_total' 
set output sciezka

#utworzenie png
#set term png enhanced size 1000,600
#set output 'web/gnuplot/out/fingers_total.png'
#***********************************

unset key
#set key samplen 8 spacing 2.15 maxcols 1 rmargin box
#set key opaque
set style data boxes
set style fill solid
set title "Wykres całkowitego użycia palców" font "Calibri,16" offset 0,1
set grid
set boxwidth 0.9
set ylabel "Liczba użyc palca" rotate parallel font "Calibri,12" offset 0.5,0
set xtics ("Palec 1" 1, "Palec 2" 2, "Palec 3" 3, "Palec 4" 4, "Palec 5" 5, "Palec 6" 6, "Palec 7" 7, "Palec 8" 8, "Palec 9" 9, "Palec 10" 10) font "Calibri,12"
set xtics nomirror
plot [0:11][0:*] 'web/gnuplot/data/fingers_total' u (1):1 t "Palec 1" lc rgb "#E6B53C",\
		'' u (1):1:1 w labels offset 0,1,\
		'' u (2):2 t "Palec 2" lc rgb "#AF7760",\
		'' u (2):2:2 w labels offset 0,1,\
		'' u (3):3 t "Palec 3" lc rgb "#591E00",\
		'' u (3):3:3 w labels offset 0,1,\
		'' u (4):4 t "Palec 4" lc rgb "#002D56",\
		'' u (4):4:4 w labels offset 0,1,\
		'' u (5):5 t "Palec 5" lc rgb "#00718F",\
		'' u (5):5:5 w labels offset 0,1,\
		'' u (6):6 t "Palec 6" lc rgb "#009B7B",\
		'' u (6):6:6 w labels offset 0,1,\
		'' u (7):7 t "Palec 7" lc rgb "#3F4B01",\
		'' u (7):7:7 w labels offset 0,1,\
		'' u (8):8 t "Palec 8" lc rgb "#850D6F",\
		'' u (8):8:8 w labels offset 0,1,\
		'' u (9):9 t "Palec 9" lc rgb "#CA2E93",\
		'' u (9):9:9 w labels offset 0,1,\
		'' u (10):10 t "Palec 10" lc rgb "#D20800",\
		'' u (10):10:10 w labels offset 0,1
		
		
#pause -1