#wywolanie z konsoli: sciezka_do_gnuplot.exe -e "sciezka=path" "sciezka_do_skryptu"
# gnuplot.exe -e "sciezka=path" "web/gnuplot/in/_WYKRES_COMFORT.dem"

reset
set encoding utf8
#wyswietlenie w gnuplocie
#set term win size 1000,600

#utworzenie canvasa w js
set term canvas size 1000,600 name 'comfort' 
set output sciezka

#utworzenie png
#set term png enhanced size zakres_x,600
#set output 'web/gnuplot/out/comfort.png'
#***********************************

stats 'web/gnuplot/data/comfort' u 1 prefix "COMFORT"
zakres_x=(COMFORT_records+COMFORT_outofrange+COMFORT_invalid+COMFORT_blank)

#skalowanie osi y
ytics_min=COMFORT_min-(COMFORT_max-COMFORT_min)/((COMFORT_max-COMFORT_min)/15)
ytics_max=COMFORT_max+(COMFORT_max-COMFORT_min)/((COMFORT_max-COMFORT_min)/15)
ytics_freq=(ytics_max-ytics_min)/15

set ytics nomirror ytics_min,ytics_freq,ytics_max format "%.2f" textcolor rgb "#3F4B01"
set yrange [ytics_min:ytics_max]
#------------------------------
#skalowanie osi x
if((floor(zakres_x/15))>=1){
	set xtics floor(zakres_x/15)
} else {
	set xtics 1
}


set arrow 2 from COMFORT_index_max, graph 0.94 to COMFORT_index_max, COMFORT_max front
set label 2 sprintf("Max=%g dla x=%d", COMFORT_max, COMFORT_index_max) at COMFORT_index_max, graph 0.96 front

if(COMFORT_index_max<zakres_x/2){
	set label 3 sprintf("Avg=%g", COMFORT_mean) at zakres_x*10/15, graph 0.96 front
	set label 4 sprintf("StdDev=%g", COMFORT_stddev) at zakres_x*12/15, graph 0.96 front
	if(COMFORT_index_max<zakres_x*3/5 && COMFORT_index_max>zakres_x*2/5){
		set label 2 center
	} else {
		set label 2 left
	}
} else {
	set label 3 sprintf("Avg=%g", COMFORT_mean) at zakres_x/15, graph 0.96 front
	set label 4 sprintf("StdDev=%g", COMFORT_stddev) at zakres_x*3/15, graph 0.96 front
	if(COMFORT_index_max<zakres_x*3/5 && COMFORT_index_max>zakres_x*2/5){
		set label 2 center
	} else {
		set label 2 right
	}
}

set arrow 1 from COMFORT_index_min, graph 0.06 to COMFORT_index_min, COMFORT_min front
set label 1 sprintf("Min=%g dla x=%d", COMFORT_min, COMFORT_index_min) at COMFORT_index_min, graph 0.04 front

if(COMFORT_index_min<zakres_x*2/5){
	set label 1 left
} else {
	if(COMFORT_index_min>zakres_x*3/5){
		set label 1 right
	} else {
		set label 1 center
	}
}


unset key
#set key samplen 8 spacing 2.15 maxcols 1 rmargin box
#set key opaque
set style data lines
set title "Wykres dyskomfortu pisania kolejnych znaków" font "Calibri,16" offset 0,1
set grid
set ylabel "Dyskomfort" rotate parallel font "Calibri,12" textcolor rgb "#3F4B01" offset 1,0
set xlabel "Znak w korpusie" rotate parallel font "Calibri,12"
plot [0:zakres_x-1] (COMFORT_mean-COMFORT_stddev) with filledcurves y1=(COMFORT_mean+COMFORT_stddev) t "Odchylenie std" lt 1 lc rgb "#cccccc",\
				COMFORT_mean t "Srednia" lc rgb "#888888",\
				'web/gnuplot/data/comfort' t "Dyskomfort" lc rgb "#3F4B01"

#pause -1
