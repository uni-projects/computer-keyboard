#wywolanie z konsoli: sciezka_do_gnuplot.exe -e "sciezka=path" "sciezka_do_skryptu"
# gnuplot.exe -e 'sciezka=path' "web/gnuplot/in/_WYKRES_ZAGREGOWANY.dem"

reset
set encoding utf8
#wyswietlenie w gnuplocie
#set term win size 1000,600

#utworzenie canvasa w js
set term canvas size 1000,600 name 'fingers' 
set output sciezka

#utworzenie png
#set term png enhanced size zakres_x*4,600
#set output 'web/gnuplot/out/fingers.png'
#unset grid
#set xtics zakres_x/40
#***********************************

stats 'web/gnuplot/data/fingers' u 1 prefix "FINGERS"
zakres_x=(FINGERS_records+FINGERS_outofrange+FINGERS_invalid+FINGERS_blank)

unset key
#set key samplen 8 spacing 2.15 rmargin maxcols 1 box
#set key opaque
set style data lines
set title "Wykres procentowego udziału palców w trakcie pisania zadanego korpusu" font "Calibri,16" offset 0,1
set style fill solid 1.0 noborder
set grid front
set ytics 10

if((floor(zakres_x/15))>=1){
	set xtics floor(zakres_x/15)
} else {
	set xtics 1
}

set ylabel "% udział palców" rotate parallel font "Calibri,12" offset 2,0
set xlabel "Znak w korpusie" font "Calibri,12"
plot [0:zakres_x-1][0:*] 'web/gnuplot/data/fingers' u 1:(0):((100.*(column(2)))/(column(12))) t "Palec 1" w filledcu lc rgb "#E6B53C",\
		'' u 1:((100.*(column(2)))/(column(12))):((100.*((column(3))+(column(2))))/(column(12))) t "Palec 2" w filledcu lc rgb "#AF7760",\
		'' u 1:((100.*((column(3))+(column(2))))/(column(12))):((100.*((column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 3" w filledcu lc rgb "#591E00",\
		'' u 1:((100.*((column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 4" w filledcu lc rgb "#002D56",\
		'' u 1:((100.*((column(5))+(column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 5" w filledcu lc rgb "#00718F",\
		'' u 1:((100.*((column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 6" w filledcu lc rgb "#009B7B",\
		'' u 1:((100.*((column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 7" w filledcu lc rgb "#3F4B01",\
		'' u 1:((100.*((column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(9))+(column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 8" w filledcu lc rgb "#850D6F",\
		'' u 1:((100.*((column(9))+(column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(10))+(column(9))+(column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 9" w filledcu lc rgb "#CA2E93",\
		'' u 1:((100.*((column(10))+(column(9))+(column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))):((100.*((column(11))+(column(10))+(column(9))+(column(8))+(column(7))+(column(6))+(column(5))+(column(4))+(column(3))+(column(2))))/(column(12))) t "Palec 10" w filledcu lc rgb "#D20800"	
		
#pause -1		
