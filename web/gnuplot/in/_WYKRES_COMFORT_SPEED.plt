#wywolanie z konsoli: sciezka_do_gnuplot.exe -e 'sciezka=path' "sciezka_do_skryptu"
# gnuplot.exe -e 'sciezka=path' "web/gnuplot/in/_WYKRES_COMFORT_SPEED.dem"

reset
set encoding utf8
#wyswietlenie w gnuplocie
#set term win size 1000,600

#utworzenie canvasa w js
set term canvas size 1000,600 name 'comfort_speed' 
set output sciezka

#utworzenie png
#set term png enhanced size zakres_x,600
#set output 'web/gnuplot/out/comfort_speed.png'
#***********************************

stats 'web/gnuplot/data/speed' u 1 prefix "SPEED"
stats 'web/gnuplot/data/comfort' u 1 prefix "COMFORT"

speed_x=(SPEED_records+SPEED_outofrange+SPEED_invalid+SPEED_blank)
comfort_x=(COMFORT_records+COMFORT_outofrange+COMFORT_invalid+COMFORT_blank)

if(speed_x==comfort_x){
zakres_x=speed_x
} else {
	if(speed_x>comfort_x){
		zakres_x=speed_x
	} else {
		zakres_x=comfort_x
	}
}

#skalowanie osi y
ytics_min=SPEED_min-(SPEED_max-SPEED_min)/((SPEED_max-SPEED_min)/15)
ytics_max=SPEED_max+(SPEED_max-SPEED_min)/((SPEED_max-SPEED_min)/15)
ytics_freq=(ytics_max-ytics_min)/15

set ytics nomirror ytics_min,ytics_freq,ytics_max format "%.2f" textcolor rgb "#FF0000" 
set yrange [ytics_min:ytics_max]

#skalowanie osi y2
y2tics_min=COMFORT_min-(COMFORT_max-COMFORT_min)/((COMFORT_max-COMFORT_min)/15)
y2tics_max=COMFORT_max+(COMFORT_max-COMFORT_min)/((COMFORT_max-COMFORT_min)/15)
y2tics_freq=(y2tics_max-y2tics_min)/15

set y2tics nomirror y2tics_min,y2tics_freq,y2tics_max format "%.2f" textcolor rgb "#3F4B01"
set y2range [y2tics_min:y2tics_max]


if((floor(zakres_x/15))>=1){
	set xtics floor(zakres_x/15)
} else {
	set xtics 1
}


unset key
#set key samplen 8 spacing 2.15 maxcols 1 rmargin box
#set key opaque
set style data lines
set title "Wykres czasu trwania i dyskomfortu pisania kolejnych znaków" font "Calibri,16" offset 0,1
set grid
set ylabel "Czas trwania" rotate parallel font "Calibri,12" textcolor rgb "#FF0000" offset 1,0
set y2label "Dyskomfort" rotate parallel font "Calibri,12" textcolor rgb "#3F4B01"
set xlabel "Znak w korpusie" rotate parallel font "Calibri,12"
plot [0:zakres_x-1] 'web/gnuplot/data/speed' axes x1y1 t "Czas trwania" lc rgb "#FF0000",\
					'web/gnuplot/data/comfort' axes x1y2 t "Dyskomfort" lc rgb "#3F4B01"


#pause -1