#wywolanie z konsoli: sciezka_do_gnuplot.exe -e "sciezka=path" "sciezka_do_skryptu"
# gnuplot.exe -e "sciezka=path" "web/gnuplot/in/_WYKRES_SPEED.dem"

reset
set encoding utf8
#wyswietlenie w gnuplocie
#set term win size 1000,600

#utworzenie canvasa w js
set term canvas size 1000,600 name 'speed' 
set output sciezka

#utworzenie png
#set term png enhanced size zakres_x,600
#set output 'web/gnuplot/out/speed.png'
#***********************************

stats 'web/gnuplot/data/speed' u 1 prefix "SPEED"
zakres_x=(SPEED_records+SPEED_outofrange+SPEED_invalid+SPEED_blank)

#skalowanie osi y
ytics_min=SPEED_min-(SPEED_max-SPEED_min)/((SPEED_max-SPEED_min)/15)
ytics_max=SPEED_max+(SPEED_max-SPEED_min)/((SPEED_max-SPEED_min)/15)
ytics_freq=(ytics_max-ytics_min)/15

set ytics nomirror ytics_min,ytics_freq,ytics_max format "%.2f" textcolor rgb "#FF0000"
set yrange [ytics_min:ytics_max]


set arrow 2 from SPEED_index_max, graph 0.94 to SPEED_index_max, SPEED_max front
set label 2 sprintf("Max=%g dla x=%d", SPEED_max, SPEED_index_max) at SPEED_index_max, graph 0.96 front

if(SPEED_index_max<zakres_x/2){
	set label 3 sprintf("Avg=%g", SPEED_mean) at zakres_x*10/15, graph 0.96 front
	set label 4 sprintf("StdDev=%g", SPEED_stddev) at zakres_x*12/15, graph 0.96 front
	if(SPEED_index_max<zakres_x*3/5 && SPEED_index_max>zakres_x*2/5){
		set label 2 center
	} else {
		set label 2 left
	}
} else {
	set label 3 sprintf("Avg=%g", SPEED_mean) at zakres_x/15, graph 0.96 front
	set label 4 sprintf("StdDev=%g", SPEED_stddev) at zakres_x*3/15, graph 0.96 front
	if(SPEED_index_max<zakres_x*3/5 && SPEED_index_max>zakres_x*2/5){
		set label 2 center
	} else {
		set label 2 right
	}
}

set arrow 1 from SPEED_index_min, graph 0.06 to SPEED_index_min, SPEED_min front
set label 1 sprintf("Min=%g dla x=%d", SPEED_min, SPEED_index_min) at SPEED_index_min, graph 0.04 front

if(SPEED_index_min<zakres_x*2/5){
	set label 1 left
} else {
	if(SPEED_index_min>zakres_x*3/5){
		set label 1 right
	} else {
		set label 1 center
	}
}


if((floor(zakres_x/15))>=1){
	set xtics floor(zakres_x/15)
} else {
	set xtics 1
}


unset key
#set key samplen 8 spacing 2.15 maxcols 1 rmargin box
#set key opaque
set style data lines
set title "Wykres czasu trwania pisania kolejnych znaków" font "Calibri,16" offset 0,1
set grid
set ylabel "Czas trwania" rotate parallel font "Calibri,12" textcolor rgb "#FF0000" offset 1,0
set xlabel "Znak w korpusie" rotate parallel font "Calibri,12"
plot [0:zakres_x-1] (SPEED_mean-SPEED_stddev) with filledcurves y1=(SPEED_mean+SPEED_stddev) t "Odchylenie std" lt 1 lc rgb "#cccccc",\
				SPEED_mean t "Srednia" lc rgb "#888888",\
				'web/gnuplot/data/speed' t "Dyskomfort" lc rgb "#FF0000"

#pause -1