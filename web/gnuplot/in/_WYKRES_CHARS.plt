#wywolanie z konsoli: sciezka_do_gnuplot.exe -e "sciezka=path" "sciezka_do_skryptu"

reset
set encoding utf8
#wyswietlenie w gnuplocie
#set term win size 1000,600

#utworzenie canvasa w js
set term canvas size 1252,600 name 'chars' 
set output sciezka

#utworzenie png
#set term png enhanced size 1000,600
#set output 'web/gnuplot/out/chars.png'
#***********************************

stats 'web/gnuplot/data/chars' u 2 prefix "CHARS"
zakres_x=(CHARS_records+CHARS_outofrange+CHARS_invalid+CHARS_blank)

unset key
#set key samplen 8 spacing 2.15 maxcols 1 rmargin box
#set key opaque
set style data boxes
set style fill solid
set title "Wykres występowania znaków w korpusie" font "Calibri,16" offset 0,1
set grid
set boxwidth 0.7
set ylabel "Liczba wystąpień" rotate parallel font "Calibri,12" offset 0.5,0
#font_size=ceil(500/zakres_x)
set xtics font "Calibri" nomirror

plot [-0.5:zakres_x-0.5][0:*] for[i=0:zakres_x-1] 'web/gnuplot/data/chars' every ::(i)::(i) u (i):(column(2)):xtic(3) notitle,\
		'' u :2:2 w labels offset 0,1 notitle,
		

		

		
#pause -1