var config = {
    keyWidth: 5, // === gridWidth * 5
    keyHeight: 5, // === gridHeight * 5
    gridWidth: 1,
    gridHeight: 1,
    unitSize: 10,

    // configurable
    gridCols: 1,
    gridRows: 1
};

var signInfo = {
    signType: document.querySelectorAll(".sign-type"),
    specialContent: document.querySelector(".special-content"),
    alphanumericalContent: document.querySelector(".alphanumerical-content")
};

var grid = document.querySelector(".grid");
var gridPos = {
    x: grid.offsetLeft,
    y: grid.offsetTop
};

var gridDimension = {
    cols: document.querySelector("#cols"),
    rows: document.querySelector("#rows")
};

var newFile = false;

var keySeqNo = 0;
var keys = [];

var keyPos;
var keyHover = document.createElement("div");
keyHover.className = "keyHover";
keyHover.style.width = config.keyWidth * config.unitSize + "px";
keyHover.style.height = config.keyHeight * config.unitSize + "px";
keyHover.style.display = "none";
grid.appendChild(keyHover);

var keyDialog = document.querySelector("#key-dialog");
var keyDialogContent = document.querySelector("#key-dialog .dialog-content");
var submitDialog = document.querySelector(".btn-submit");
var removeBtn = document.querySelector(".btn-remove");
var saveBtn = document.querySelector(".btn-save");
var canvas = document.getElementsByTagName("canvas")[0];

window.alert = function (message) {
    window.onresize = centerAlert;
    keyHover.style.display = "none";

    function centerAlert() {
        var alertBg = document.getElementById("alertBg");
        alertBg.style.width = (parseInt(grid.style.width) > window.innerWidth) ? grid.style.width : window.innerWidth + "px";
        alertBg.style.height = document.body.offsetHeight + "px";
        var alertContent = document.getElementById("alertContent");
        alertContent.style.left = 0.5 * window.innerWidth + window.scrollX + "px";
        alertContent.style.top = 0.5 * window.innerHeight + window.scrollY + "px";
    }

    var alertBg = document.createElement("div");
    alertBg.setAttribute("id", "alertBg");
    alertBg.setAttribute("class", "alertBg");
    document.body.appendChild(alertBg);

    var box = document.createElement('div');
    box.setAttribute("id", "alertContent");
    box.innerHTML = '<div id="button2" ><div id="alert"><span class="alertMessage">' + message + '</span></div><input type="button" id="closeAlertBtn" value="OK" ></div>';
    document.body.appendChild(box);
    centerAlert();

    var closeAlertBtn = document.getElementById("closeAlertBtn");
    closeAlertBtn.addEventListener("click", resume, false);

    function resume() {
        document.body.removeChild(alertBg);
        document.body.removeChild(box);
        keyHover.style.display = "block";
    }
}

function Key(config) {
    this.isSet = config.isSet;
    this.seqNo = config.seqNo;
    this.sign = config.sign;
    this.isDefaultFinger = config.isDefaultFinger;
}

function checkIfSpecialKey(sign) {
    if (sign === ";") {
        return "key_semicolon";
    } else if (sign === ".") {
        return "key_point";
    } else if (sign === ",") {
        return "key_comma";
    } else if (sign === "[") {
        return "key_left_square_bracket";
    } else if (sign === "]") {
        return "key_right_square_bracket";
    } else if (sign === "\\") {
        return "key_backslash";
    } else if (sign === "\/") {
        return "key_slash";
    } else if (sign === "-") {
        return "key_minus";
    } else if (sign === "=") {
        return "key_equals";
    } else if (sign === "'") {
        return "key_apostrophe";
    } else if (sign === "`") {
        return "key_bend_apostrophe";
    } else if (sign === "+") {
        return "key_plus";
    } else if (sign === "*") {
        return "key_asterisk";
    } else if (sign === "!") {
        return "key_exclamation";
    } else if (sign === "@") {
        return "key_at";
    } else if (sign === "#") {
        return "key_hash";
    } else if (sign === "$") {
        return "key_dollar";
    } else if (sign === "%") {
        return "key_percent";
    } else if (sign === "^") {
        return "key_caret";
    } else if (sign === "&") {
        return "key_ampersand";
    } else if (sign === "(") {
        return "key_left_parenthesis";
    } else if (sign === ")") {
        return "key_right_parenthesis";
    } else if (sign === "{") {
        return "key_left_brace";
    } else if (sign === "}") {
        return "key_right_brace";
    } else if (sign === "?") {
        return "key_question";
    } else if (sign === ":") {
        return "key_colon";
    } else if (sign === "\"") {
        return "key_quotation";
    } else if (sign === "<") {
        return "key_less";
    } else if (sign === ">") {
        return "key_greater";
    } else if (sign === "~") {
        return "key_tilde";
    } else if (
        sign === "backspace" ||
            sign === "tab" ||
            sign === "space" ||
            sign === "alt" ||
            sign === "alt2" ||
            sign === "ctrl" ||
            sign === "ctrl2" ||
            sign === "shift" ||
            sign === "shift2" ||
            sign === "cmd" ||
            sign === "capslock" ||
            sign === "enter" ||
            sign === "esc" ||
            sign === "0" ||
            sign === "1" ||
            sign === "2" ||
            sign === "3" ||
            sign === "4" ||
            sign === "5" ||
            sign === "6" ||
            sign === "7" ||
            sign === "8" ||
            sign === "9") {
        return "key_" + sign;
    } else {
        return -1;
    }
}

function checkIfSpecialChar(sign) {
    if (sign === "0" ||
        sign === "1" ||
        sign === "2" ||
        sign === "3" ||
        sign === "4" ||
        sign === "5" ||
        sign === "6" ||
        sign === "7" ||
        sign === "8" ||
        sign === "9" ||
        sign === "backspace" ||
        sign === "tab" ||
        sign === "space") {
        return "key_" + sign;
    } else if (
        sign === "alt" ||
            sign === "alt2" ||
            sign === "ctrl" ||
            sign === "ctrl2" ||
            sign === "shift" ||
            sign === "shift2" ||
            sign === "capslock" ||
            sign === "cmd" ||
            sign === "esc") {
        return -1;
    } else if (sign === ".") {
        return "key_point";
    } else if (sign === ",") {
        return "key_comma";
    } else if (sign === ";") {
        return "key_semicolon";
    } else if (sign === "/") {
        return "key_slash";
    } else if (sign === "\\") {
        return "key_backslash";
    } else if (sign === "[") {
        return "key_left_square_bracket";
    } else if (sign === "]") {
        return "key_right_square_bracket";
    } else if (sign === "'") {
        return "key_apostrophe";
    } else if (sign === "`") {
        return "key_bend_apostrophe";
    } else if (sign === "+") {
        return "key_plus";
    } else if (sign === "*") {
        return "key_asterisk";
    } else if (sign === "-") {
        return "key_minus";
    } else if (sign === "=") {
        return "key_equals";
    } else if (sign === "enter") {
        return "key_" + sign;
    } else if (sign === "!") {
        return "key_exclamation";
    } else if (sign === "@") {
        return "key_at";
    } else if (sign === "#") {
        return "key_hash";
    } else if (sign === "$") {
        return "key_dollar";
    } else if (sign === "%") {
        return "key_percent";
    } else if (sign === "^") {
        return "key_caret";
    } else if (sign === "&") {
        return "key_ampersand";
    } else if (sign === "(") {
        return "key_left_parenthesis";
    } else if (sign === ")") {
        return "key_right_parenthesis";
    } else if (sign === "{") {
        return "key_left_brace";
    } else if (sign === "}") {
        return "key_right_brace";
    } else if (sign === "?") {
        return "key_question";
    } else if (sign === ":") {
        return "key_colon";
    } else if (sign === "\"") {
        return "key_quotation";
    } else if (sign === "<") {
        return "key_less";
    } else if (sign === ">") {
        return "key_greater";
    } else if (sign === "~") {
        return "key_tilde";
    } else {
        return 1;
    }
}

Key.prototype.saveKeyMap = function () {
    var left = ((this.position.x - gridPos.x) / (config.unitSize * config.gridWidth)) + 1,
        top = ((this.position.y - gridPos.y) / (config.unitSize * config.gridHeight)) + 1,
        sign;
    sign = checkIfSpecialKey(this.sign);
    if (sign === -1) {
        return "(defkey :" + this.sign.toLowerCase() + " " + left + " " + top + ")";
    } else if (sign !== -1) {
        return "(defkey :" + sign + " " + left + " " + top + ")";
    }
};

Key.prototype.saveCharMap = function () {
    var sign;

    sign = checkIfSpecialChar(this.sign);
    if (isNaN(this.finger)) this.finger = "undefined";
    if (sign === -1) {
        return -1;
    } else if (sign === 1) {
        return "(compose #\\" + this.sign.toLowerCase() + " (:" + this.sign.toLowerCase() + " :f" + this.finger + "))";
    } else if (sign === "key_enter") {
        return "(compose #\\newline" + " (:" + sign + " :f" + this.finger + "))";
    } else {
        return "(compose #\\" + this.sign + " (:" + sign + " :f" + this.finger + "))";
    }
};

Key.prototype.saveDefaultKeys = function () {
    var left = ((this.position.x - gridPos.x) / config.unitSize) + 1,
        top = ((this.position.y - gridPos.y) / config.unitSize) + 1;

    return "(deffinger :f" + this.defaultFinger + " " + left + " " + top + ")";
}

function readConfFile(ev) {
    var files = ev.target.files,
        contents;

    if (files) {
        for (var i = 0, file; file = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = function (ev) {
                contents = ev.target.result;
                clearGrid();
                parseFile(contents);
            };
            reader.readAsText(file);
        }
    } else {
        alert("Błąd ładowania danych.");
    }
}

function clearGrid() {
    var keysOnGrid = document.querySelectorAll(".keyOnGrid"),
        index = 0;

    if (index < keysOnGrid.length) {
        [].forEach.call(
            keysOnGrid, function (keysOnGrid) {
                keysOnGrid.parentNode.removeChild(keysOnGrid);
            }
        );
        keys.length = 0;
    }
}

function parseFile(contents) {
    contents = contents.replace(/[\(\):]+/g, "");
    var rows = contents.split("\n");
    newFile = true;
    var newKey;

    for (var i in rows) {
        var data = rows[i].split(" ");
        var key = new Key({
            active: false,
            isSet: true,
            seqNo: keySeqNo,
            isDefaultFinger: false
        });

        switch (data[1]) {
            case "key_alt":
                key.sign = "alt";
                break;
            case "key_alt2":
                key.sign = "alt2";
                break;
            case "key_backspace":
                key.sign = "backspace";
                break;
            case "key_capslock":
                key.sign = "capslock";
                break;
            case "key_cmd":
                key.sign = "cmd";
                break;
            case "key_ctrl":
                key.sign = "ctrl";
                break;
            case "key_ctrl2":
                key.sign = "ctrl2";
                break;
            case "key_enter":
                key.sign = "enter";
                break;
            case "key_esc":
                key.sign = "esc";
                break;
            case "key_shift":
                key.sign = "shift";
                break;
            case "key_shift2":
                key.sign = "shift2";
                break;
            case "key_space":
                key.sign = "space";
                break;
            case "key_tab":
                key.sign = "tab";
                break;
            case "key_comma":
                key.sign = ",";
                break;
            case "key_point":
                key.sign = ".";
                break;
            case "key_semicolon":
                key.sign = ";";
                break;
            case "key_slash":
                key.sign = "/";
                break;
            case "key_backslash":
                key.sign = "\\";
                break;
            case "key_backspace":
                key.sign = "backspace";
                break;
            case "key_left_square_bracket":
                key.sign = "[";
                break;
            case "key_right_square_bracket":
                key.sign = "]";
                break;
            case "key_apostrophe":
                key.sign = "'";
                break;
            case "key_bend_apostrophe":
                key.sign = "`";
                break;
            case "key_plus":
                key.sign = "+";
                break;
            case "key_asterisk":
                key.sign = "*";
                break;
            case "key_minus":
                key.sign = "-";
                break;
            case "key_equals":
                key.sign = "=";
                break;
            case "key_exclamation":
                key.sign = "!";
                break;
            case "key_at":
                key.sign = "@";
                break;
            case "key_hash":
                key.sign = "#";
                break;
            case "key_dollar":
                key.sign = "$";
                break;
            case "key_percent":
                key.sign = "%";
                break;
            case "key_caret":
                key.sign = "^";
                break;
            case "key_ampersand":
                key.sign = "&";
                break;
            case "key_left_parenthesis":
                key.sign = "(";
                break;
            case "key_right_parenthesis":
                key.sign = ")";
                break;
            case "key_left_brace":
                key.sign = "{";
                break;
            case "key_right_brace":
                key.sign = "}";
                break;
            case "key_question":
                key.sign = "?";
                break;
            case "key_colon":
                key.sign = ":";
                break;
            case "key_quotation":
                key.sign = "\"";
                break;
            case "key_less":
                key.sign = "<";
                break;
            case "key_greater":
                key.sign = ">";
                break;
            case "key_tilde":
                key.sign = "~";
                break;
            case "key_0":
                key.sign = "0";
                break;
            case "key_1":
                key.sign = "1";
                break;
            case "key_2":
                key.sign = "2";
                break;
            case "key_3":
                key.sign = "3";
                break;
            case "key_4":
                key.sign = "4";
                break;
            case "key_5":
                key.sign = "5";
                break;
            case "key_6":
                key.sign = "6";
                break;
            case "key_7":
                key.sign = "7";
                break;
            case "key_8":
                key.sign = "8";
                break;
            case "key_9":
                key.sign = "9";
                break;

            default:
                key.sign = data[1];
        }

        key.position = {
            x: ((parseInt(data[2]) - 1) * config.unitSize) + parseInt(gridPos.x),
            y: ((parseInt(data[3]) - 1) * config.unitSize) + parseInt(gridPos.y)
        }

        newKey = createKeyFromFile(key);
        keySeqNo++;

        if (key.sign === "undefined") {
            key.sign = "";
            document.querySelector("#keyOnGrid-" + key.seqNo + " .sign").innerText = "";
        }

        if (data[4] != null) {
            key.finger = parseInt(data[4].slice(1));
            colorKeys(newKey, key);
        }

        if (data[5] != null) {
            key.isDefaultFinger = true;
            key.defaultFinger = parseInt(data[5].slice(2));
            newKey.style.opacity = 0.7;
            newKey.style.border = "3px solid #1B9FB3";
        }
    }

    updateSettingsValue();
    onConfigChange(newFile);
    drawGrid();
    newFile = false;
}

function createKeyFromFile(key) {
    var newKey = createKey(key.position);

    keys.push(key);

    var sign = document.createElement("span");
    sign.className = "sign";
    sign.innerHTML = key.sign.toLowerCase();

    newKey.appendChild(sign);
    newKey.addEventListener("click", function () {
        openKeyDialog(newKey, key);
    }, false);

    return newKey;
}

function drawGrid() {
    grid.style.width = config.gridCols * config.gridWidth * config.unitSize + "px";
    grid.style.height = config.gridRows * config.gridHeight * config.unitSize + 1 + "px";

    //grid width and height
    var bw = parseInt(grid.style.width);
    var bh = parseInt(grid.style.height);
    //size of canvas
    var cw = bw + 1;
    var ch = bh + 1;

    var canvas = document.getElementsByTagName("canvas")[0];
    canvas.setAttribute("width", "" + cw);
    canvas.setAttribute("height", "" + ch);
    grid.appendChild(canvas);

    var context = canvas.getContext("2d");
    var x;

    for (x = 0; x <= bw; x += parseInt(config.gridWidth * config.unitSize)) {
        context.moveTo(0.5 + x, 0);
        context.lineTo(0.5 + x, bh);
    }

    for (x = 0; x <= bh; x += parseInt(config.gridHeight * config.unitSize)) {
        context.moveTo(0, 0.5 + x);
        context.lineTo(bw, 0.5 + x);
    }

    context.strokeStyle = "#e3e3e3";
    context.stroke();
}

function updateConfig() {
    config.gridCols = gridDimension.cols.value;
    config.gridRows = gridDimension.rows.value;
    grid.style.display = "block";
}

function onConfigChange(newFile) {
    var max = {
        x: 0,
        y: 0
    };

    if ((gridDimension.cols.value > 500) || (gridDimension.rows.value > 500)) {
        alert("Zbyt duży rozmiar.");
        gridDimension.cols.value = (config.gridCols < 501) ? config.gridCols : 0;
        gridDimension.rows.value = (config.gridRows < 501) ? config.gridRows : 0;
        return;
    }

    updateConfig();
    grid.style.width = config.gridCols * config.unitSize + "px";
    grid.style.height = config.gridRows * config.unitSize + "px";

    max = countMaxKeyPos(max);
    if (newFile) {
        updateGrid(max);
    } else if (parseInt(grid.style.height) < max.y) {
        if (config.gridRows * config.unitSize !== 0) {
            max.x = config.gridCols * config.unitSize;
        }
        updateGrid(max);
    } else if ((parseInt(grid.style.width) < max.x)) {
        if (config.gridRows * config.unitSize !== 0) {
            max.y = config.gridRows * config.unitSize;
        }
        updateGrid(max);
    }
    drawGrid();
    newFile = false;
}

function countMaxKeyPos(max) {
    var maxX, maxY;

    for (var i in keys) {
        maxX = keys[i].position.x + (config.keyWidth * config.unitSize) - gridPos.x;
        maxY = keys[i].position.y + (config.keyHeight * config.unitSize) - gridPos.y;

        if (maxX > max.x) {
            max.x = maxX;
        }

        if (maxY > max.y) {
            max.y = maxY;
        }
    }

    return max;
}

function updateGrid(max) {
    grid.style.width = max.x + "px";
    grid.style.height = max.y + "px";

    var gridWidth = parseInt(grid.style.width),
        gridHeight = parseInt(grid.style.height);

    config.gridCols = gridWidth / config.unitSize;
    config.gridRows = gridHeight / config.unitSize;

    gridDimension.cols.value = gridWidth / parseInt(config.unitSize);
    gridDimension.rows.value = gridHeight / parseInt(config.unitSize);
}

function createKey(pointPos) {
    var keyOnGrid = document.createElement("div");
    keyOnGrid.id = "keyOnGrid-" + keySeqNo;
    keyOnGrid.className = "keyOnGrid";
    keyOnGrid.style.width = config.keyWidth * config.unitSize + "px";
    keyOnGrid.style.height = config.keyHeight * config.unitSize + "px";
    keyOnGrid.style.position = "absolute";
    keyOnGrid.style.left = pointPos.x + "px";
    keyOnGrid.style.top = pointPos.y + "px";

    keyPos = {
        top: keyOnGrid.style.top,
        left: keyOnGrid.style.left
    };
    grid.appendChild(keyOnGrid);

    return keyOnGrid;
}

function findPointAtPosition(x, y) {
    var pointPos = {
        x: 0,
        y: 0
    };

    x = x - gridPos.x - Math.floor((config.keyWidth * config.unitSize) / 2);
    y = y - gridPos.y - Math.floor((config.keyHeight * config.unitSize) / 2);

    x = Math.round(x / config.unitSize) * config.unitSize;
    y = Math.round(y / config.unitSize) * config.unitSize;

    pointPos.x = x + gridPos.x;
    pointPos.y = y + gridPos.y;

    return pointPos;
}

function drawKey(ev) {
    var gridHeight = parseInt(grid.style.height);
    var gridWidth = parseInt(grid.style.width);
    var keyOnGrid;
    var keyObject;
    var pointPos = findPointAtPosition(ev.pageX, ev.pageY);

    if (((pointPos.y >= gridPos.y)
        && (pointPos.y <= ((gridPos.y + gridHeight) - Math.floor(config.keyHeight * config.unitSize))))
        && ((pointPos.x >= gridPos.x)
        && (pointPos.x <= ((gridPos.x + gridWidth) - Math.floor(config.keyWidth * config.unitSize))))) {

        if (isKeyOnGrid(pointPos)) {
            alert("Klawisz w tym miejscu istnieje.");
            return;
        }
        keyOnGrid = createKey(pointPos);
        keyObject = initKey(keySeqNo);
        keyHover.style.display = "none";


        keyOnGrid.addEventListener("click", function () {
            openKeyDialog(keyOnGrid, keyObject);
        }, false);

        keySeqNo = keySeqNo + 1;
    } else {
        alert("Umieść klawisz na siatce.");
    }
}

function initKey(keySeqNo) {
    var key = new Key({
        isSet: false,
        seqNo: keySeqNo,
        isDefaultFinger: false
    });
    key.position = {
        y: parseInt(keyPos.top),
        x: parseInt(keyPos.left)
    };
    keys.push(key);

    return key;
}

function isKeyOnGrid(pointPos) {
    var isKey;
    for (var i in keys) {
        var keyLeft = parseInt(keys[i].position.x),
            keyRight = keyLeft + parseInt(config.unitSize * config.keyWidth),
            keyTop = parseInt(keys[i].position.y),
            keyBottom = keyTop + parseInt(config.unitSize * config.keyHeight);

        if ((pointPos.x >= (keyLeft - config.keyWidth * config.unitSize + 2)) && (pointPos.x < keyRight)) {
            if ((pointPos.y >= (keyTop - config.keyHeight * config.unitSize + 3)) && (pointPos.y < keyBottom)) {
                isKey = true;
            } else {
                isKey = false;
            }
        } else {
            isKey = false;
        }

        if (isKey) {
//            keyHover.style.backgroundColor = "";
//            keyHover.style.backgroundColor = "#FF9494";
            return isKey;
        }
    }
    isKey = false;

//    keyHover.style.background = "";
//    keyHover.style.backgroundColor = "#35b8ca";

    return isKey;
}

function isKeyOnHover(ev) {
    var isKey;
    for (var i in keys) {
        var keyLeft = parseInt(keys[i].position.x),
            keyRight = keyLeft + parseInt(config.unitSize * config.keyWidth),
            keyTop = parseInt(keys[i].position.y),
            keyBottom = keyTop + parseInt(config.unitSize * config.keyHeight);

        if ((ev.pageX >= keyLeft) && (ev.pageX < keyRight)) {
            if ((ev.pageY >= keyTop) && (ev.pageY < keyBottom)) {
                isKey = true;
            } else {
                isKey = false;
            }
        } else {
            isKey = false;
        }

        if (isKey) {
            return isKey;
        }
    }
    isKey = false;
    return isKey;
}

function checkIfDefaultExists(key, settings) {
    var countDefault = 0,
        countUndefined = 0,
        seqNo;

    for (var i in keys) {
        if ((keys[i].isDefaultFinger) && (settings.defaultFinger === String(keys[i].defaultFinger))) {
            countDefault++;
            seqNo = keys[i].seqNo;
        }
        if (keys[i].defaultFinger === undefined) {
            countUndefined++;
        }
    }

    if (((countDefault === 1) && (key.seqNo === seqNo))
        || (countUndefined === keys.length)) {
        return false;
    } else {
        return true;
    }
}

function saveKeySettings(keyOnGrid, key) {
    var keyEl = document.querySelector("#" + keyOnGrid.id);
    var defaultExists = false;

    if (key.active) {
        var settings = updateSettingsValue();
        (settings.alpha !== "") ? (key.sign = settings.alpha.toLowerCase()) : (key.sign = settings.special);
        key.finger = settings.finger;
        key.isDefaultFinger = settings.isDefaultFinger;
        key.defaultFinger = settings.defaultFinger;

        if (key.isDefaultFinger) {
            defaultExists = checkIfDefaultExists(key, settings);
            if (defaultExists) {
                alert("Ten palec jest już przypisany.");
                key.defaultFinger = undefined;
                return false;
            }
        }

        colorKeys(keyOnGrid, key);

        // overwrite existing letter
        if (keyEl.childNodes[0] !== undefined) {
            var span = keyEl.querySelectorAll(".sign");
            span[0].parentNode.removeChild(span[0]);
        }

        var sign = document.createElement("span");
        sign.className = "sign";
        sign.innerHTML = key.sign.toLowerCase();
        keyEl.appendChild(sign);

        key.active = false;
    }
}

function colorKeys(keyOnGrid, key) {
    keyOnGrid.style.boxShadow = "none";
    switch (key.finger) {
        case 1:
            keyOnGrid.style.background = "#E6B53C";
            break;
        case 2:
            keyOnGrid.style.background = "#AF7760";
            break;
        case 3:
            keyOnGrid.style.background = "#591E00";
            break;
        case 4:
            keyOnGrid.style.background = "#002D56";
            break;
        case 5:
            keyOnGrid.style.background = "#00718F";
            break;
        case 6:
            keyOnGrid.style.background = "#009B7B";
            break;
        case 7:
            keyOnGrid.style.background = "#3F4B01";
            break;
        case 8:
            keyOnGrid.style.background = "#850D6F";
            break;
        case 9:
            keyOnGrid.style.background = "#CA2E93";
            break;
        case 10:
            keyOnGrid.style.background = "#D20800";
            break;
        default:
            keyOnGrid.style.background = "#000";
    }

    if (key.isDefaultFinger) {
        keyOnGrid.style.opacity = 0.7;
        keyOnGrid.style.border = "3px solid #1B9FB3";
    } else {
        keyOnGrid.style.opacity = 1;
        keyOnGrid.style.border = "none";
    }
}

function closeKeyDialog(keyOnGrid, key) {
    var saved = saveKeySettings(keyOnGrid, key);
    if (saved !== false) {
        keyDialog.className = "is-hidden";
    } else {
        keyDialog.className = "";
    }
}

function centerKeyDialog() {
    keyDialogContent.style.left = 0.5 * window.innerWidth + window.scrollX + "px";
    keyDialogContent.style.top = 0.4 * window.innerHeight + window.scrollY + "px";
    keyDialog.style.width = (parseInt(grid.style.width) > window.innerWidth) ? grid.style.width : window.innerWidth + "px";
    keyDialog.style.height = document.body.offsetHeight + "px";
}

function removeKey(keyOnGrid, key) {
    var keyEl = document.querySelector("#" + keyOnGrid.id);
    var index;

    if (key.active) {
        keyEl.parentNode.removeChild(keyEl);
        index = keys.indexOf(key);
        keys.splice(index, 1);
        keyDialog.className = "is-hidden";
    }
    key.active = false;
}

function updateSettingsValue() {
    var settings = {
        alpha: document.querySelector("#alphanumerical").value,
        special: document.querySelector("#special").value,
        finger: parseInt(document.querySelector("#finger").value),
        isDefaultFinger: document.querySelector("#is-default-finger").checked,
        defaultFinger: document.querySelector("#default-finger").value
    }

    return settings;
}

function validateAlphaInput() {
    var alphaInput = document.querySelector("#alphanumerical"),
        errorMsg = document.querySelector("#error-message");
    alphaInput.addEventListener("change", function () {
        var str = alphaInput.value.trim();
        if (str != "") {
            var regx = /^[A-Za-z0-9]+$/;
            if (!regx.test(str)) {
                for (var i in str) {
                    if (!regx.test(str[i])) {
                        str = str.replace(str[i], " ");
                    }
                }
                str = str.split(' ').join('');
                alphaInput.value = str;
                alphaInput.className = "has-error";
                errorMsg.style.display = "block";
            } else {
                alphaInput.className = "";
                errorMsg.style.display = "none";
            }
        } else {
            alphaInput.value = "";
        }
    });
}

function openKeyDialog(keyOnGrid, key) {
    var specials = document.querySelectorAll("#special option"),
        isSpecial = false,
        alphanumCheckbox = document.querySelector("#sign-alphanumerical"),
        specialCheckbox = document.querySelector("#sign-special");
    key.active = true;
    keyDialog.className = "";
    centerKeyDialog();
    validateAlphaInput();

    if (key.isSet === false) {
        document.querySelector("#alphanumerical").value = "";
        document.querySelector("#special").value = "";
        document.querySelector("#finger").value = "";
        document.querySelector("#is-default-finger").checked = false;
        document.querySelector("#default-finger").value = "";
    } else {
        document.querySelector("#alphanumerical").value = key.sign;
        document.querySelector("#special").value = key.sign;
        document.querySelector("#finger").value = key.finger;
        document.querySelector("#is-default-finger").checked = key.isDefaultFinger;
        document.querySelector("#default-finger").value = key.defaultFinger;
    }

    for (var i in specials) {
        if ((key.sign === specials[i].value) && (key.sign !== undefined)) {
            isSpecial = true;
        }
    }

    if (key.isSet === false) {
        specialCheckbox.checked = false;
        alphanumCheckbox.checked = false;
    } else if (isSpecial) {
        specialCheckbox.checked = true;
    } else {
        alphanumCheckbox.checked = true;
    }

    checkSignTypeChecked();

    submitDialog.addEventListener("click", function () {
        closeKeyDialog(keyOnGrid, key);
    }, false);
    removeBtn.addEventListener("click", function () {
        removeKey(keyOnGrid, key);
    }, false)

    key.isSet = true;
}

function onClick(ev) {
    drawKey(ev);
}

function isCursorOnGrid(ev) {
    var isOnGrid = true;
    if ((ev.pageX > gridPos.x + parseInt(grid.style.width)) ||
        (ev.pageX < gridPos.x) ||
        (ev.pageY > gridPos.y + parseInt(grid.style.height)) ||
        (ev.pageY < gridPos.y)) {

        keyHover.style.display = "none";
        isOnGrid = false;
    } else {
        isOnGrid = true;
    }

    return isOnGrid;
}

function onGridHover(pointPos, ev) {
    (config.gridCols < 5 || config.gridRows < 2) ? (keyHover.style.display = "none") : (keyHover.style.display = "block");
    keyHover.style.top = pointPos.y + "px";
    keyHover.style.left = pointPos.x + "px";

    if (isKeyOnHover(ev)) {
        keyHover.style.display = "none";
    } else {
        (config.gridCols < 5 || config.gridRows < 2) ? (keyHover.style.display = "none") : (keyHover.style.display = "block");
    }

    if (isCursorOnGrid(ev) === false) {
        keyHover.style.display = "none";
    }
}

function onKeyHover(ev) {
    keyHover.style.top = ev.pageY - Math.floor((config.keyHeight * config.unitSize) / 2) + "px";
    keyHover.style.left = ev.pageX - Math.floor((config.keyWidth * config.unitSize) / 2) + "px";
    keyPos = {
        top: keyHover.style.top,
        left: keyHover.style.left
    };

    isKeyOnHover(ev);
}

function saveLayout() {
    var toSaveKeyMap = [],
        toSaveCharMap = [],
        toSaveDefaultKeys = [],
        toSaveKeyboard = [],
        combo = document.querySelectorAll("#combos .myCombo");

    for (var i in keys) {
        if (isNaN(keys[i].finger)) {
            keys[i].finger = "undefined";
        }
        if (parseInt(i) === keys.length - 1) {
            toSaveKeyMap.push(keys[i].saveKeyMap());
            if (keys[i].isDefaultFinger) {
                toSaveKeyboard.push(keys[i].saveKeyMap() + " f:" + keys[i].finger + " fd:" + keys[i].defaultFinger);
            } else {
                toSaveKeyboard.push(keys[i].saveKeyMap() + " f:" + keys[i].finger);
            }
        } else {
            toSaveKeyMap.push(keys[i].saveKeyMap() + "\r\n");
            if (keys[i].isDefaultFinger) {
                toSaveKeyboard.push(keys[i].saveKeyMap() + " f:" + keys[i].finger + " fd:" + keys[i].defaultFinger + "\r\n");
            } else {
                toSaveKeyboard.push(keys[i].saveKeyMap() + " f:" + keys[i].finger + "\r\n");
            }
        }
        if (keys[i].saveCharMap() !== -1) {
            toSaveCharMap.push(keys[i].saveCharMap() + "\r\n");
        }
        if (keys[i].isDefaultFinger) {
            toSaveDefaultKeys.push(keys[i].saveDefaultKeys() + "\r\n");
        }
    }

    [].forEach.call(
        combo, function (combo) {
            var comboKey = combo.querySelectorAll(".myCombo .combo-key"),
                comboFinger = combo.querySelectorAll(".myCombo .finger"),
                composedLetter = combo.querySelector(".myCombo .composed-letter").value;

            if (composedLetter === "") {
                return;
            }

            toSaveCharMap.push("(compose #\\" + composedLetter);

            for (var i in combo) {
                if (comboKey[i].value === "") {
                    toSaveCharMap.push(")" + "\r\n");
                    return;
                } else {
                    var sign = checkIfSpecialKey(comboKey[i].value);
                    if (sign !== -1) {
                        toSaveCharMap.push(" (:" + sign + " :f" +
                            comboFinger[i].value + ")");
                    } else {
                        toSaveCharMap.push(" (:" + comboKey[i].value + " :f" +
                            comboFinger[i].value + ")");
                    }
                }
            }
        }
    );

    var keyMap = new Blob(toSaveKeyMap, {
        type: "text/html;charset=utf-8"
    });

    var charMap = new Blob(toSaveCharMap, {
        type: "text/html;charset=utf-8"
    });

    var defaultKeys = new Blob(toSaveDefaultKeys, {
        type: "text/html;charset=utf-8"
    });

    var keyboard = new Blob(toSaveKeyboard, {
        type: "text/html;charset=utf-8"
    });

    saveAs(keyMap, "keymap.lisp");
    saveAs(charMap, "charmap.lisp");
    saveAs(defaultKeys, "fingermap.lisp");
    saveAs(keyboard, "keyboard.lisp");
}

function checkSignTypeChecked() {
    var checked = false;

    for (var i = 0; i < signInfo.signType.length; i++) {
        if (signInfo.signType[i].checked) {
            if (signInfo.signType[i].id === "sign-special") {
                signInfo.alphanumericalContent.className = "alphanumerical-content sign-alphanumerical is-hidden";
                signInfo.alphanumericalContent.querySelector("input").value = "";
                signInfo.specialContent.className = "special-content sign-special";
            } else if (signInfo.signType[i].id === "sign-alphanumerical") {
                signInfo.specialContent.className = "special-content sign-special is-hidden";
                signInfo.specialContent.querySelector("select").value = "";
                signInfo.alphanumericalContent.className = "alphanumerical-content sign-alphanumerical";
            }
            checked = true;
        }
    }

    if (checked === false) {
        signInfo.alphanumericalContent.checked = true;
        document.querySelector("#sign-alphanumerical").checked = true;
        signInfo.alphanumericalContent.className = "alphanumerical-content sign-alphanumerical";
        signInfo.alphanumericalContent.querySelector("input").value = "";
        signInfo.specialContent.className = "special-content sign-special is-hidden";
        signInfo.specialContent.querySelector("select").value = "";
    }
}

// COMBO BOX
(function () {
    var wrapper = document.querySelector(".combo-wrapper"),
        addCombo = document.querySelector(".add-combo"),
        index = 0;

    function getForm(index) {
        var comboForm = document.createElement("form"),
            anchor = document.createElement("a"),
            span = document.createElement("span"),
            plus = document.createElement("plus"),
            inputs = [],
            fingerSelect = [],
            fingerOption = [],
            composeLetter;

        comboForm.id = "myCombo" + index;
        comboForm.className = "myCombo";
        anchor.className = "remove";
        anchor.innerText = "Usuń combo";
        anchor.textContent = "Usuń combo";
        composeLetter = document.createElement("input");
        composeLetter.className = "composed-letter";
        span.innerText = "=";
        span.className = "equals-sign";
        comboForm.appendChild(composeLetter);
        comboForm.appendChild(span);

        for (var i = 0; i < 4; i++) {
            inputs[i] = document.createElement("input");
            inputs[i].id = "combo-key" + i;
            inputs[i].className = "combo-key";
            span[i] = document.createElement("span");
            span[i].className = "finger-label";
            span[i].innerText = "palec: ";
            fingerSelect[i] = document.createElement("select");
            fingerSelect[i].id = "finger" + i;
            fingerSelect[i].className = "finger";

            for (var j = 1; j <= 10; j++) {
                fingerOption[j] = document.createElement("option");
                fingerOption[j].innerText = j;
                fingerOption[j].textContent = j;
                fingerSelect[i].appendChild(fingerOption[j]);
            }
            plus[i] = document.createElement("span");
            plus[i].className = "plus-sign";
            plus[i].innerText = "+";
            comboForm.appendChild(inputs[i]);
            comboForm.appendChild(span[i]);
            comboForm.appendChild(fingerSelect[i]);
            if (i < 3) {
                comboForm.appendChild(plus[i]);
            }
        }
        comboForm.appendChild(anchor);
        return comboForm;
    }

    function fillComboInputs(ev) {
        var keyPressed = "",
            keyCode = ev.which,
            keyLocation = ev.keyLocation;

        console.log(ev.keyCode);
        if (ev.shiftKey === true) {
            if (keyCode >= 65 && keyCode <= 90) {
                return keyPressed;
            } else {
                keyLocation === 1 ? keyPressed = "shift" : keyPressed = "shift2";
            }
        } else if ((ev.ctrlKey === true) && (ev.altKey === false)) {
            keyLocation === 1 ? keyPressed = "ctrl" : keyPressed = "ctrl2";
        } else if (ev.altKey === true) {
            if (keyCode >= 65 && keyCode <= 90) {
                return keyPressed;
            } else if ((keyCode === 18)) {
                keyLocation === 1 ? keyPressed = "alt" : keyPressed = "alt2";
            }
        } else if (ev.keyCode === 9) {
            keyPressed = "tab";
        } else if (ev.keyCode === 20) {
            keyPressed = "capslock";
        } else if (ev.keyCode === 91) {
            keyPressed = "cmd";
        } else if (ev.keyCode === 93) {
            keyPressed = "cmd";
        } else {
            if (keyCode >= 65 && keyCode <= 90) {
                return keyPressed;
            }
        }

        return keyPressed;
    }

    function fadeIn(elem, speed) {
        if (elem.style) {
            elem.style.opacity = '0';
        }
        window.fadetimer = setInterval(function () {
                if (elem.style.opacity < 1) {
                    elem.style.opacity = +(elem.style.opacity) + .2;
                }
            },
            speed);
    }

    addCombo.addEventListener("click", function () {
        var comboForm = getForm(++index),
            remove = comboForm.querySelector(".remove"),
            keyValue = "";

        remove.addEventListener("click", function () {
            comboForm.parentNode.removeChild(comboForm);
        }, false);

        wrapper.appendChild(comboForm);
        fadeIn(comboForm, 30);

        var keyInput = document.querySelectorAll(".combo-wrapper .combo-key");
        [].forEach.call(
            keyInput, function (keyInput) {
                keyInput.addEventListener("keydown", function (ev) {
                    keyValue = fillComboInputs(ev);
                    keyInput.value = keyValue;
                }, false);
            }
        );
    }, false);
})();

(function () {
    gridDimension.cols.addEventListener("change", function () {
        onConfigChange(newFile);
    });
    gridDimension.rows.addEventListener("change", function () {
        onConfigChange(newFile);
    });
    document.getElementById("fileinput").addEventListener("change", readConfFile, false);
    keyHover.addEventListener("click", onClick, false);
    canvas.addEventListener("click", onClick, false);
    grid.addEventListener("mousemove", function (ev) {
        var pointPos = findPointAtPosition(ev.pageX, ev.pageY);
        onGridHover(pointPos, ev);
    }, false);
    keyHover.addEventListener("mousemove", onKeyHover, false);
    saveBtn.addEventListener("click", saveLayout, false);
    grid.addEventListener("mousemove", function (ev) {
        var pointPos = findPointAtPosition(ev.pageX, ev.pageY);
        isKeyOnGrid(pointPos);
    }, false);

    for (var i = 0; i < signInfo.signType.length; i++) {
        signInfo.signType[i].addEventListener("change", checkSignTypeChecked, false);
    }
})();




