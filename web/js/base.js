(function () {
    var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);

    var menuItems = document.querySelectorAll("nav .menu li a");
    [].forEach.call(
        menuItems, function (menuItems) {
            if (menuItems.getAttribute("href") === pgurl || menuItems.getAttribute("href") === '') {
                menuItems.className = "is-active";
            } else {
                menuItems.className = "";
            }
        }
    );
})();