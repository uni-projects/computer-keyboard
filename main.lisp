
(asdf:oos 'asdf:load-op :cffi)
(asdf:oos 'asdf:load-op :cl-fad)

(defpackage :dead-corpse
  (:use :common-lisp :cffi :cl-fad)
  (:export :fast-start
	   :initialize
	   :parse-corpus
	   :deffinger
	   :defkey
	   :compose
	   :finalize
	   :*aggregation*))

(in-package :dead-corpse)

(defparameter *workspace* (pathname-directory-pathname *load-pathname*)
  "Ścieżka do katalogu ze źródłami")

(defparameter *foreign-file*
  (merge-pathnames
   *workspace* "foreign.lisp"))
(defparameter *scoring-file*
  (merge-pathnames
   *workspace* "scoring.lisp"))

(defvar *keysyms*)
(defvar *fingers*)
(defvar *reverse-lookup-fingers*)
(defvar *aggregation*)

(load *foreign-file*)
(load *scoring-file*)
;(initialize-libraries)

;; Idiotyczny hack
(defvar *idiotic-hack* (make-hash-table)
  "Hack na statyczne wpisanie symboli")
(dolist (v '(:f1 :f2 :f3 :f4 :f5 :f6 :f7 :f8 :f9 :f10))
  (setf (gethash v *idiotic-hack*)
	(1+ (hash-table-count *idiotic-hack*))))

;; load fingermap
(defun deffinger (fingersym &rest coords)
  (check-type fingersym symbol)
  (let ((finger-id
	 (if (nth-value 1 (gethash fingersym *idiotic-hack*))
	     (nth-value 0 (gethash fingersym *idiotic-hack*))
	     (+ 11 (hash-table-count *fingers*))))) ; Start from 11 if not in *idiotic_hack* table
    (setf (gethash fingersym *fingers*)
	  (cons finger-id
		(list :finger fingersym :coords coords))
	  (gethash finger-id *reverse-lookup-fingers*)
	  fingersym)))

  ;; (setf (gethash
  ;; 	 (1+ (hash-table-count *fingers*))
  ;; 	 *fingers*))
  ;; (setf (gethash fingersym *fingers*)
  ;; 	(cons
  ;; 	 (1+ (hash-table-count *fingers*))
  ;; 	 (list :finger fingersym :coords coords))))

;; load keymap
(defun defkey (keysym &rest coord)
  (the symbol keysym)
  (reg-key (if (nth-value 1 (gethash keysym *keysyms*))
	       (gethash keysym *keysyms*)
	       (setf (gethash keysym *keysyms*)
		     (1+ (hash-table-count *keysyms*))))
	   (foreign-alloc
	    :unsigned-int
	    :count (1+ (length coord))
	    :initial-contents (append coord '(0)))))

;; load charmap
(defmacro compose (char &rest keys)
  (reg-char
   (char-code char)
   (foreign-alloc 
    :unsigned-int
    :count (1+ (length keys))
    :initial-contents 
    (append 
     (mapcar 
      (lambda (k)
  	(if (symbolp (car k))
  	    (gethash (car k) *keysyms*)
  	    (car k)))
      keys)
     '(0)))
   (foreign-alloc
    :unsigned-int
    :count (1+ (length keys))
    :initial-contents
    (append 
     (mapcar
      (lambda (k)
	(if (nth-value 1 (gethash (cadr k) *fingers*))
	    (car (gethash (cadr k) *fingers*))
	    (error (format nil 
			   "Palec ~A nie jest zdefiniowany"
			   (cadr k)))))
      keys)
     '(0)))))


(defun parse-corpus (file &key (path ""))
  (with-foreign-string (destination-prefix path)
    (let ((ret (init-stats *aggregation* destination-prefix)))
      (when (< ret 0)
	(error (format nil "Inicjalizacja zakończona niepowodzeniem")))))
  (register-callbacks (callback speed-function-wrapper)
		      (callback comfort-function-wrapper))
  (handler-bind ((sb-int:stream-decoding-error
		  (lambda (condition)
		    (declare (ignore condition))
		    (invoke-restart 'sb-int:attempt-resync))))
    (with-open-file (s file)
      (do ((c (read-char s nil) (read-char s nil)))
	  ((not c))
	(static-stats-char (char-code c))
	(parse-character (char-code c))
	(flush)))
    (finalize-stats)))

(defun initialize (&rest files)
  (setf *keysyms* (make-hash-table))
  (setf *fingers* (make-hash-table))
  (setf *reverse-lookup-fingers* (make-hash-table))
  (setf *aggregation* 50)
  (init-hash 65536 128)
  (mapcar #'load files))

(defun finalize ()
  (free-hash))

(defun fast-start ()
  (initialize 
   (merge-pathnames
    *workspace* "fingermap-qwerty.lisp")
   (merge-pathnames
    *workspace* "keymap-qwerty.lisp")
   (merge-pathnames
    *workspace* "charmap-qwerty.lisp"))
  (parse-corpus
   (merge-pathnames
    *workspace* "dead.corpse"))
  (finalize))

;; ;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; Wczytywanie znaków ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;
;; (defparameter *keycodes*
;;   (make-hash-table)
;;   "Zdefiniowane klawisze")
;; (defparameter *compose*
;;   (make-hash-table)
;;   "Zdefiniowane klawisze")





;; (defun keycode (code &rest keysym)
;;   (setf (gethash code *keycodes*) keysym))

;; (load "keys.keymap")
;; ;(load "functions.lisp")
