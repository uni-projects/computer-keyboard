#DEBUG=-g -D DEBUG=0
DEBUG=
CFLAGS=-Wall

all: utils libs

run: all
	sbcl --load webserver.lisp --eval "(in-package :dead-corpse)"

dirs:
	mkdir -p libs web/configs/ web/corpses/ web/functions/ web/gnuplot/data/

libs: dirs libparse libstat

libtest: test.h test.c
	gcc -c -fpic test.c ${CFLAGS} ${DEBUG}
	gcc -shared -o libs/libtest.so test.o

libparse: libstat initparse.h initparse.c
	gcc -c -fpic initparse.c ${CFLAGS} ${DEBUG}
	gcc -shared -o libs/libparse.so initparse.o stat.o

libstat: stat.h stat.c
	gcc -c -fpic stat.c ${CFLAGS} ${DEBUG}
	gcc -shared -o libs/libstat.so stat.o

utils: utils.c
	gcc -c -fpic utils.c ${CFLAGS} ${DEBUG}
	gcc -shared -o libs/utils.so utils.o

clean:
	rm -f *.o *.so *.dll libs/* chars comfort fingers fingers_total speed

