(defkey :key_tab 1 6) f:2
(defkey :key_shift 1 11) f:1
(defkey :key_ctrl 1 16) f:1
(defkey :j 7 1) f:1
(defkey :o 7 6) f:1
(defkey :q 7 11) f:1
(defkey :v 12 1) f:2
(defkey :a 12 6) f:2 fd:1
(defkey :z 12 11) f:2
(defkey :w 17 1) f:3
(defkey :i 17 6) f:3 fd:2
(defkey :c 22 1) f:3
(defkey :e 22 6) f:3 fd:3
(defkey :key_left_square_bracket 22 11) f:3
(defkey :b 27 1) f:4
(defkey :d 27 6) f:4 fd:4
(defkey :key_right_square_bracket 27 11) f:4
(defkey :g 32 1) f:4
(defkey :t 32 6) f:4
(defkey :key_backslash 32 11) f:4 fd:5
(defkey :key_backspace 23 16) f:5
(defkey :key_alt 16 16) f:5
(defkey :y 38 1) f:7
(defkey :h 38 6) f:7
(defkey :key_cmd 35 16) f:5
(defkey :u 43 1) f:7
(defkey :r 43 6) f:7 fd:7
(defkey :m 48 1) f:8
(defkey :n 48 6) f:8 fd:8
(defkey :k 53 1) f:8
(defkey :l 53 6) f:8 fd:9
(defkey :p 58 1) f:9
(defkey :s 58 6) f:9 fd:10
(defkey :x 63 1) f:10
(defkey :f 63 6) f:10
(defkey :key_shift2 69 11) f:10
(defkey :key_enter 69 6) f:9
(defkey :key_ctrl2 69 16) f:10
(defkey :key_capslock 9 16) f:2
(defkey :key_bend_apostrophe 17 11) f:3
(defkey :key_semicolon 38 11) f:7 fd:6
(defkey :key_apostrophe 43 11) f:7
(defkey :key_comma 48 11) f:8
(defkey :key_point 53 11) f:8
(defkey :key_slash 58 11) f:9
(defkey :key_space 43 16) f:6
(defkey :key_alt2 49 16) f:6