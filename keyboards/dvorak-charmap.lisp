(compose #\` (:key_bend_apostrophe :f1))
(compose #\~ (:key_shift :f5) (:key_bend_apostrophe :f4))
(compose #\1 (:key_1 :f1))
(compose #\! (:key_shift :f5) (:key_1 :f3))
(compose #\2 (:key_2 :f2))
(compose #\@ (:key_shift :f5) (:key_2 :f4))
(compose #\3 (:key_3 :f3))
(compose #\# (:key_shift :f1) (:key_3 :f3))
(compose #\4 (:key_4 :f4))
(compose #\$ (:key_shift :f4) (:key_4 :f7))
(compose #\5 (:key_5 :f4))
(compose #\% (:key_shift :f4) (:key_5 :f7))
(compose #\6 (:key_6 :f7))
(compose #\^ (:key_shift :f4) (:key_6 :f7))
(compose #\7 (:key_7 :f7))
(compose #\& (:key_shift :f4) (:key_7 :f7))
(compose #\8 (:key_8 :f8))
(compose #\* (:key_shift :f4) (:key_8 :f7))
(compose #\9 (:key_9 :f9))
(compose #\( (:key_shift :f4) (:key_9 :f7))
(compose #\0 (:key_0 :f10))
(compose #\) (:key_shift :f4) (:key_0 :f8))
(compose #\[ (:key_left_square_bracket :f7))
(compose #\{ (:key_shift :f4) (:key_left_square_bracket :f7))
(compose #\] (:key_right_square_bracket :f8))
(compose #\} (:key_shift :f4) (:key_right_square_bracket :f8))
(compose #\backspace (:key_backspace :f8))
(compose #\' (:key_apostrophe :f1))
(compose #\, (:key_comma :f2))
(compose #\. (:key_point :f3))
(compose #\p (:p :f4))
(compose #\P (:key_shift :f1) (:p :f4))
(compose #\y (:y :f4))
(compose #\Y (:key_shift :f1) (:y :f4))
(compose #\f (:f :f7))
(compose #\F (:key_shift :f1) (:f :f7))
(compose #\g (:g :f7))
(compose #\G (:key_shift :f1) (:g :f7))
(compose #\c (:c :f8))
(compose #\C (:key_shift :f1) (:c :f8))
(compose #\r (:r :f9))
(compose #\R (:key_shift :f1) (:r :f9))
(compose #\l (:l :f10))
(compose #\L (:key_shift :f1) (:l :f10))
(compose #\/ (:key_slash :f7))
(compose #\= (:key_equals :f8))
(compose #\\ (:key_backslash :f9))
(compose #\a (:a :f1))
(compose #\A (:key_shift :f1) (:a :f2))
(compose #\o (:o :f2))
(compose #\O (:key_shift :f1) (:o :f2))
(compose #\e (:e :f3))
(compose #\E (:key_shift :f1) (:e :f3))
(compose #\u (:u :f4))
(compose #\U (:key_shift :f1) (:u :f4))
(compose #\i (:i :f4))
(compose #\I (:key_shift :f1) (:i :f7))
(compose #\d (:d :f7))
(compose #\D (:key_shift :f1) (:d :f7))
(compose #\h (:h :f7))
(compose #\H (:key_shift :f1) (:h :f7))
(compose #\t (:t :f8))
(compose #\T (:key_shift :f1) (:t :f8))
(compose #\n (:n :f9))
(compose #\N (:key_shift :f1) (:n :f9))
(compose #\s (:s :f10))
(compose #\S (:key_shift :f4) (:s :f7))
(compose #\- (:key_minus :f10))
(compose #\tab (:key_tab :f2))
(compose #\; (:key_semicolon :f2))
(compose #\q (:q :f3))
(compose #\Q (:key_shift :f1) (:q :f3))
(compose #\j (:j :f4))
(compose #\J (:key_shift :f1) (:j :f4))
(compose #\k (:k :f4))
(compose #\K (:key_shift :f1) (:k :f4))
(compose #\x (:x :f7))
(compose #\X (:key_shift :f1) (:x :f7))
(compose #\b (:b :f7))
(compose #\B (:key_shift :f1) (:b :f7))
(compose #\m (:m :f7))
(compose #\M (:key_shift :f1) (:m :f7))
(compose #\w (:w :f8))
(compose #\W (:key_shift :f3) (:w :f7))
(compose #\v (:v :f9))
(compose #\V (:key_shift :f3) (:v :f8))
(compose #\z (:z :f10))
(compose #\Z (:key_shift :f3) (:z :f9))
(compose #\space (:key_space :f5))
(compose #\newline (:key_enter :f10))
(compose #\_ (:key_shift :f4) (:key_minus :f8))
(compose #\+ (:key_shift :f4) (:key_equals :f8))
(compose #\| (:key_shift :f4) (:key_backslash :f9))
(compose #\: (:key_shift :f1) (:key_semicolon :f2))
(compose #\< (:key_shift :f1) (:key_comma :f3))
(compose #\> (:key_shift :f1) (:key_point :f3))
(compose #\? (:key_shift :f4) (:key_slash :f7))
(compose #\" (:key_shift :f1) (:key_apostrophe :f2))
(compose #\Ą (:key_alt2 :f6) (:key_shift :f1) (:a :f2))
(compose #\ą (:key_alt2 :f6) (:a :f1))
(compose #\Ę (:key_alt2 :f6) (:key_shift :f1) (:e :f3))
(compose #\ę (:key_alt2 :f6) (:e :f3))
(compose #\Ó (:key_alt2 :f6) (:key_shift :f1) (:o :f2))
(compose #\ó (:key_alt2 :f6) (:o :f2))
(compose #\Ń (:key_alt2 :f6) (:key_shift :f1) (:n :f7))
(compose #\ń (:key_alt2 :f6) (:n :f7))
(compose #\Ć (:key_alt2 :f6) (:key_shift :f1) (:c :f7))
(compose #\ć (:key_alt2 :f6) (:c :f7))
(compose #\Ź (:key_alt2 :f6) (:key_shift :f1) (:x :f4))
(compose #\ź (:key_alt2 :f6) (:x :f7))
(compose #\Ż (:key_alt2 :f7) (:key_shift :f1) (:z :f9))
(compose #\ż (:key_alt2 :f7) (:z :f9))
(compose #\Ś (:key_alt2 :f6) (:key_shift :f1) (:s :f9))
(compose #\ś (:key_alt2 :f6) (:s :f9))
(compose #\Ł (:key_alt2 :f6) (:key_shift :f1) (:l :f8))
(compose #\ł (:key_alt2 :f6) (:l :f8))
