(defkey :key_esc 1 1) f:1
(defkey :key_bend_apostrophe 1 7) f:1
(defkey :key_1 7 7) f:1
(defkey :key_2 12 7) f:2
(defkey :key_3 17 7) f:3
(defkey :key_4 22 7) f:4
(defkey :key_5 27 7) f:4
(defkey :key_6 32 7) f:7
(defkey :key_7 37 7) f:7
(defkey :key_8 42 7) f:8
(defkey :key_9 47 7) f:9
(defkey :key_0 52 7) f:10
(defkey :key_minus 57 7) f:7
(defkey :key_equals 62 7) f:8
(defkey :q 9 12) f:1
(defkey :w 14 12) f:2
(defkey :e 19 12) f:3
(defkey :r 24 12) f:4
(defkey :t 29 12) f:4
(defkey :y 34 12) f:7
(defkey :u 39 12) f:7
(defkey :i 44 12) f:8
(defkey :o 49 12) f:9
(defkey :p 54 12) f:10
(defkey :key_left_square_bracket 59 12) f:7
(defkey :key_right_square_bracket 64 12) f:8
(defkey :key_backslash 69 12) f:9
(defkey :a 10 17) f:1 fd:1
(defkey :s 15 17) f:2 fd:2
(defkey :d 20 17) f:3 fd:3
(defkey :f 25 17) f:4 fd:4
(defkey :g 30 17) f:4
(defkey :h 35 17) f:7
(defkey :j 40 17) f:7 fd:7
(defkey :k 45 17) f:8 fd:8
(defkey :l 50 17) f:9 fd:9
(defkey :key_semicolon 55 17) f:10 fd:10
(defkey :key_apostrophe 60 17) f:10
(defkey :key_enter 67 17) f:10
(defkey :key_shift 3 22) f:1
(defkey :key_tab 1 12) f:2
(defkey :key_capslock 2 17) f:1
(defkey :z 12 22) f:2
(defkey :x 17 22) f:3
(defkey :c 22 22) f:4
(defkey :v 27 22) f:4 fd:5
(defkey :b 32 22) f:7
(defkey :n 37 22) f:7 fd:6
(defkey :m 42 22) f:7
(defkey :key_comma 47 22) f:8
(defkey :key_point 52 22) f:9
(defkey :key_slash 57 22) f:10
(defkey :key_ctrl 2 27) f:1
(defkey :key_alt 13 27) f:5
(defkey :key_space 32 27) f:5
(defkey :key_backspace 68 7) f:8
(defkey :key_alt2 49 27) f:6
(defkey :key_ctrl2 68 27) f:10
(defkey :key_shift2 66 22) f:10