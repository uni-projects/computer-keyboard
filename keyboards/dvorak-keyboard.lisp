(defkey :key_bend_apostrophe 1 7) f:1
(defkey :key_1 7 7) f:1
(defkey :key_2 12 7) f:2
(defkey :key_3 17 7) f:3
(defkey :key_4 22 7) f:4
(defkey :key_5 27 7) f:4
(defkey :key_6 32 7) f:7
(defkey :key_7 37 7) f:7
(defkey :key_8 42 7) f:8
(defkey :key_9 47 7) f:9
(defkey :key_0 52 7) f:10
(defkey :key_left_square_bracket 57 7) f:7
(defkey :key_right_square_bracket 62 7) f:8
(defkey :key_backspace 68 7) f:8
(defkey :key_apostrophe 9 12) f:1
(defkey :key_comma 14 12) f:2
(defkey :key_point 19 12) f:3
(defkey :p 24 12) f:4
(defkey :y 29 12) f:4
(defkey :f 34 12) f:7
(defkey :g 39 12) f:7
(defkey :c 44 12) f:8
(defkey :r 49 12) f:9
(defkey :l 54 12) f:10
(defkey :key_slash 59 12) f:7
(defkey :key_equals 64 12) f:8
(defkey :key_backslash 69 12) f:9
(defkey :a 10 17) f:1 fd:1
(defkey :o 15 17) f:2 fd:2
(defkey :e 20 17) f:3 fd:3
(defkey :u 25 17) f:4 fd:4
(defkey :i 30 17) f:4
(defkey :d 35 17) f:7
(defkey :h 40 17) f:7 fd:7
(defkey :t 45 17) f:8 fd:8
(defkey :n 50 17) f:9 fd:9
(defkey :s 55 17) f:10 fd:10
(defkey :key_minus 60 17) f:10
(defkey :key_shift 3 22) f:1
(defkey :key_tab 1 12) f:2
(defkey :key_capslock 2 17) f:1
(defkey :key_semicolon 12 22) f:2
(defkey :q 17 22) f:3
(defkey :j 22 22) f:4
(defkey :k 27 22) f:4 fd:5
(defkey :x 32 22) f:7
(defkey :b 37 22) f:7 fd:6
(defkey :m 42 22) f:7
(defkey :w 47 22) f:8
(defkey :v 52 22) f:9
(defkey :z 57 22) f:10
(defkey :key_ctrl 2 27) f:1
(defkey :key_alt 13 27) f:5
(defkey :key_space 32 27) f:5
(defkey :key_alt2 49 27) f:6
(defkey :key_shift2 66 22) f:10
(defkey :key_ctrl2 68 27) f:10
(defkey :key_enter 67 17) f:10