#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#define ZAKRES_ZNAKOW 65536

unsigned int fingers_tab[10] = {0}; // calkowity licznik uzytych palcow
unsigned int char_tab[ZAKRES_ZNAKOW] = {0}; // calkowity licznik uzytych znakow

unsigned int fingers_tab_temp[10] = {0};

//!!!!!!!!!!!!!!!!!!
unsigned int agreguj_po = 50;
//!!!!!!!!!!!!!!!!!!

// zmienne 'licznik' odpowiadaja za prawidlowe obliczanie srednich zanim napelnia sie bufory cykliczne
unsigned int licznik_komfort = 0;
unsigned int licznik_predkosci = 0;

unsigned int licznik = 0; // zlicza ilosc wywolan funkcji
char path[128];

int **zagregowana_ilosc_palcow; // bufor cykliczny zliczajacy palce
int *zagregowana_predkosc;      // bufor cykliczny zliczajacy predkosc
int *zagregowany_komfort;       // bufor cykliczny zliczajacy komfort
FILE *fp,*speed,*comfort,*chars,*fingers_total;


// wpisanie do pliku tablicy zliczajacej wszystkie znaki w korpusie 
int get_stats_char()
{
    int i;
    for(i=1;i<ZAKRES_ZNAKOW;i++)
         if(char_tab[i]>0){
              switch(i){
                     case 9:
                          if(fwprintf (chars, L"%d %d TB\n", i, char_tab[i])<0) return -1; // wpisanie do pliku
                          break;
                     case 10:
                          if(fwprintf (chars, L"%d %d NL\n", i, char_tab[i])<0) return -1; // wpisanie do pliku
                          break;
                     case 13:
                          break;
                     case 32:
                          if(fwprintf (chars, L"%d %d SP\n", i, char_tab[i])<0) return -1; // wpisanie do pliku
                          break;
					 case 34:
                          if(fwprintf (chars, L"%d %d ''\n", i, char_tab[i])<0) return -1; // wpisanie do pliku
                          break;
                     case 92:
                          if(fwprintf (chars, L"%d %d %c%c\n", i, char_tab[i], i, i)<0) return -1; // wpisanie do pliku
                          break;
                     default:
                          if(fwprintf (chars, L"%d %d %lc\n", i, char_tab[i], (wchar_t)i)<0) return -1; // wpisanie do pliku
                          break;
                     }
	}
    return 0;
}

// wpisanie do pliku tablicy zliczajacej wszystkie uzycia palcow 
int get_fingers_total()
{
    int i;
    for(i=0;i<10;i++)
         if(fprintf (fingers_total, "%d ", fingers_tab[i])<0) return -1; // wpisanie do pliku 
    return 0;
}


// inicjalizacja buforow i ustalenie parametru agreguj_po
int init_stats(unsigned int agreguj, char *sciezka){
#ifdef DEBUG
  printf("init_stats: agreguj=%d path=%s\n", agreguj, sciezka);
#endif
    
int i,j;

    if ((fp=fopen("web/gnuplot/data/fingers", "w"))==NULL) {   
     return -1;
     }
    if ((speed=fopen("web/gnuplot/data/speed", "w"))==NULL) {   
     return -1;
     }
    if ((comfort=fopen("web/gnuplot/data/comfort", "w"))==NULL) {   
     return -1;
     }
    if ((chars=fopen("web/gnuplot/data/chars", "w, ccs=UNICODE"))==NULL) {   
     return -1;
     }
    if ((fingers_total=fopen("web/gnuplot/data/fingers_total", "w"))==NULL) {   
     return -1;
     }
  
  snprintf(path, 128, "%s", sciezka);
  
  char buf[512];
  snprintf(buf, 512, "mkdir -p web/gnuplot/out/%s", path);
  system(buf);
  
  // tworzenie i zerowanie bufora cyklicznego zliczajecego palce
  zagregowana_ilosc_palcow = (int**)malloc(agreguj_po * sizeof(int*));
  if(zagregowana_ilosc_palcow==NULL) return -2;
  for(i=0;i<agreguj_po;i++){
              zagregowana_ilosc_palcow[i] = (int*)malloc(10 * sizeof(int));
              if(zagregowana_ilosc_palcow[i]==NULL) return -2;             
          }
     for(i=0;i<agreguj_po;i++)
              for(j=0;j<10;j++) 
                   zagregowana_ilosc_palcow[i][j]=0; 
             
  // tworzenie i zerowanie bufora cyklicznego zliczajecego predkosc                 
  zagregowana_predkosc = malloc(sizeof * zagregowana_predkosc * agreguj_po);
  if(zagregowana_predkosc==NULL) return -2;
  for(j=0;j<agreguj_po;j++)
       zagregowana_predkosc[j] = 0;
  
  // tworzenie i zerowanie bufora cyklicznego zliczajecego komfort
  zagregowany_komfort = malloc(sizeof * zagregowany_komfort * agreguj_po);
  if(zagregowany_komfort==NULL) return -2;
  for(j=0;j<agreguj_po;j++)
       zagregowany_komfort[j] = 0;        
  
  //!!!!!!!!!!! USTALENIE PARAMETRU AGREGUJ_PO !!!!!!!!!!
  agreguj_po=agreguj;
  
  return 0;                 
}

int finalize_stats()
{
#ifdef DEBUG
  printf("finalize_stats: agreguj=%d path=%s\n", agreguj_po, path);
#endif

  int i=0,j;
  i+=get_stats_char();
  i+=get_fingers_total();
  i+=fclose (fp);
  i+=fclose (speed);
  i+=fclose (comfort);
  i+=fclose (chars);
  i+=fclose (fingers_total);
  
  // ODPALENIE SKRYPTOW GNUPLOTA!!
  
  char buf[512],buf2[512];

  snprintf(buf2, 512, "\'web/gnuplot/out/%sfingers_total.js\'", path);
  snprintf(buf, 512, "/usr/bin/gnuplot -e 'sciezka=\"%s\"' web/gnuplot/in/_WYKRES_SLUPKOWY.plt", buf2);
  system(buf);
  
  
  snprintf(buf2, 512, "\'web/gnuplot/out/%sfingers.js\'", path);
  snprintf(buf, 512, "/usr/bin/gnuplot -e 'sciezka=\"%s\"' web/gnuplot/in/_WYKRES_ZAGREGOWANY.plt", buf2);
  system(buf);
  
  snprintf(buf2, 512, "\'web/gnuplot/out/%sspeed.js\'", path);
  snprintf(buf, 512, "/usr/bin/gnuplot -e 'sciezka=\"%s\"' web/gnuplot/in/_WYKRES_SPEED.plt", buf2);
  system(buf);
  
  snprintf(buf2, 512, "\'web/gnuplot/out/%scomfort.js\'", path);
  snprintf(buf, 512, "/usr/bin/gnuplot -e 'sciezka=\"%s\"' web/gnuplot/in/_WYKRES_COMFORT.plt", buf2);
  system(buf);
  
  snprintf(buf2, 512, "\'web/gnuplot/out/%scomfort_speed.js\'", path);
  snprintf(buf, 512, "/usr/bin/gnuplot -e 'sciezka=\"%s\"' web/gnuplot/in/_WYKRES_COMFORT_SPEED.plt", buf2);
  system(buf);
  
  snprintf(buf2, 512, "\'web/gnuplot/out/%schars.js\'", path);
  snprintf(buf, 512, "/usr/bin/gnuplot -e 'sciezka=\"%s\"' web/gnuplot/in/_WYKRES_CHARS.plt", buf2);
  system(buf);
  
  for(j=0;j<10;j++){
                    fingers_tab[j] = 0;
                    fingers_tab_temp[j] = 0;
  }
  for(j=0;j<ZAKRES_ZNAKOW;j++)
                   char_tab[j] = 0;
  
  licznik_komfort = 0;
  licznik_predkosci = 0;
  licznik = 0;
  
  
    return i;
}

// Zliczanie wszystkich znakow (statystyka)
int static_stats_char (unsigned int char_code)
{
#ifdef DEBUG
  printf("static_stats_char: char_code=%d\n", char_code);
#endif
  
  if(char_code<0 || char_code>ZAKRES_ZNAKOW-1) return -2;
       char_tab[char_code]++;

  return 0;
}



// Zliczanie wszystkich palcow (statystyka)
int dynamic_stats_fingers (unsigned int *fingers)
{
#ifdef DEBUG
  printf("dynamic_stats_fingers: fingers={ ");
  unsigned int *dbg_f = fingers;
  if(!dbg_f) {
    printf("NULL POINTER ");
  } else {
    while(*dbg_f) {
      printf("%d ", *dbg_f);
      dbg_f++;
    }
  }
  printf("}\n");
#endif

  unsigned int *f = fingers;
  int i,j;
  
  
  if(!f) {
    return -3;
  } else {
      while(*f) {
          int i=*f;
          if(i<1 || i>10) return -2; // zly numer palca
          fingers_tab[i-1]++;  // dodawanie do globalnego licznika palcow
          fingers_tab_temp[i-1]++;  // dodawanie do tymczasowego licznika palcow, potrzebnego do bufora cyklicznego
          f++;
        }
        
     for(i=agreguj_po-1;i>0;i--){ // przesuwanie bufora cyklicznego
              for(j=0;j<10;j++)                    
                   zagregowana_ilosc_palcow[i][j] = zagregowana_ilosc_palcow[i-1][j];           
          }  
     for(j=0;j<10;j++){            // dodawanie nowego wiersza do bufora cyklicznego
          zagregowana_ilosc_palcow[0][j] = fingers_tab_temp[j];
          fingers_tab_temp[j]=0;
          }
        
        
     for(i=0;i<agreguj_po;i++){ // sumowanie w buforze cyklicznym
              for(j=0;j<10;j++)                    
                   if(zagregowana_ilosc_palcow[i][j])
                        fingers_tab_temp[j]++;           
          }
     
     if(fprintf (fp, "%d ", licznik)<0) return -1;
     
     int suma=0;     
     for(j=0;j<10;j++){         // wypisanie do pliku zagregowanych palcow
              if(fprintf (fp, "%d ", fingers_tab_temp[j])<0) return -1;
              suma+=fingers_tab_temp[j];
              fingers_tab_temp[j]=0;
          }
          if(fprintf (fp, "%d\n", suma)<0) return -1; 
          
     licznik++;
     
  return 0;
  }
}

// statystyki dotyczace predkosci pisania
int dynamic_stats_speed (int time)
{
#ifdef DEBUG
  printf("dynamic_stats_speed: time=%d\n", time);
#endif

int i,ii;
float srednia=0.0;  

      for(i=agreguj_po-1;i>0;i--){ // przesuwanie bufora cyklicznego
                   zagregowana_predkosc[i] = zagregowana_predkosc[i-1];           
          }
      zagregowana_predkosc[0]=time;
      
      licznik_predkosci++;

      for(i=0;i<agreguj_po;i++){  // usrednianie zagregowanych ocen
           srednia+=zagregowana_predkosc[i];
      }
      if(licznik_predkosci<agreguj_po) // zanim napelni sie bufor                      
           srednia=srednia/licznik_predkosci; 
      else
           srednia=srednia/agreguj_po; // bufor jest juz pelny
		   
		char buf[32]; // zapewnienie ze liczby beda zapisane z kropka
		snprintf(buf, 32, "%f", srednia);
		for(ii=0;ii<32;ii++){
			if(buf[ii]==',') buf[ii]='.';
		}
	  
      if(fprintf (speed, "%s\n", buf)<0) return -1; // wpisanie do pliku

  return 0;
}

// statystyki dotyczace komfortu pisania
int dynamic_stats_comfort (int com)
{
#ifdef DEBUG
  printf("dynamic_stats_comfort: comfort=%d\n", com);
#endif

int i,ii;
float srednia=0.0;  
      for(i=agreguj_po-1;i>0;i--){ // przesuwanie bufora cyklicznego
                   zagregowany_komfort[i] = zagregowany_komfort[i-1];           
          }
      zagregowany_komfort[0]=com;
      
      licznik_komfort++;

      for(i=0;i<agreguj_po;i++){  // usrednianie zagregowanych ocen
           srednia+=zagregowany_komfort[i]; 
      } 
      if(licznik_komfort==0) return -2;
      
      if(licznik_komfort<agreguj_po)                        
           srednia=srednia/licznik_komfort;
      else
           srednia=srednia/agreguj_po;                    

		char buf[32]; // zapewnienie ze liczby beda zapisane z kropka
		snprintf(buf, 32, "%f", srednia);
		for(ii=0;ii<32;ii++)
			if(buf[ii]==',') buf[ii]='.';
		   
      if(fprintf (comfort, "%s\n", buf)<0) return -1; // wpisanie do pliku

  return 0;
}


/*
funkcje zwracaja:
          0 jezeli wszystko wykonaja poprawnie
          -1 jezeli wystapi blad z plikiem wynikowym (blad otwarcia lub zapisu)
          <-1 jezeli wystapi inny blad (null pointer, za duza/mala wartosc etc.)
          
funkcja finalize_stats() zwraca:
        0 jezeli zapisy i zamkniecia plikow przebiegna pomyslnie
        <0 jezeli wystapia bledy przy zapisie lub zamykaniu dowolnego pliku wynikowego
*/
