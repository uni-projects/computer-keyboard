(in-package :dead-corpse)		; Obowiązkowa deklaracja

(defparameter history nil
  "Zmienna przetrzymuje serię palców koniecznych do wciśnięcia poprzedniego znaku.")

(defun finger-path-to-vector (finger-position1 finger-position2)
  "Funkcja pomocnicza - zamienia dwie pozycje palca na wektor reprezentujący 
różnicę w tych pozycjach"
  (if (not finger-position2)		; jeśli drugi argument nie jest palcem
      #C(0 0)				; zwróć wektor zerowy
      (apply #'complex			; w przeciwnym wypadku zwróć różnicę pozycji palców
	     (mapcar #'-		; mapująć funkcję różnicy
		     (getf finger-position2 :coords) ; na współrzędne poszczególnych palców
		     (getf finger-position1 :coords)))))

(defun vector-dot-product (vector1 vector2)
  "Funkcja pomocnicza - zwraca produkcję kropkową dwóch wektorów w celu
obliczenia kąta"
  (realpart
   (* vector1
      (conjugate vector2))))

(defun normalize (vector)
  "Funkcja pomocnicza - normalizacja wektora"
  (/ vector (abs vector)))

(defun vector-length (vector)
  "Funkcja pomocnicza - długość wektora"
  (abs vector))

(defun vector-angle (vector1 &optional (vector2 #C(0 1)))
  "Funkcja pomocnicza - oblicza kąt pomiędzy dwoma wektorami"
  (if (zerop vector1)
      0					; Wektor nie ma kierunku, kąt zerowy
      (acos (vector-dot-product		; Oblicz kąt wg definicji
	     (normalize vector1)
	     (normalize vector2)))))

(defun distance-penalty-wrapper (distance)
  "Fragment funkcji oceny ruchu - funkcja identyczności z dystansem przebytym przez palec"
  distance)				;identity

(defun angle-penalty-wrapper (angle)
  "Fragment funkcji oceny ruchu - funkcja przekształcająca kąt na ocenę cząstkową"
  (+ 1					; Kara nie może być zerowa (najniższa wartość funkcji)
     (* 3				; Skalowanie kary za funkcję
	(if (< angle (/ pi 2))
	    (sin angle)				     ; sin(angle)
	    (sin (- (/ (* 3 pi) 4) (/ angle 2))))))) ; sin(3pi/4 + angle/2)

(defun speed-score (path-vector &key (lately-used nil))
  "Funkcja pomocnicza - wylicza wynik dla jednego palca"
  (+ (cond
       (lately-used 3)	                ; najgorszy przypadek - palec przed chwilą był ruszany
       ((zerop path-vector) 1)		; najlepszy przypadek - palec może pozostać na miejscu
       (T 2))				; palec musi się przemieścić w celu wykonania ruchu
     (* (distance-penalty-wrapper	; iloczyn funkcji kary dla odległości i kąta
	 (vector-length path-vector))
	(angle-penalty-wrapper
	 (vector-angle path-vector)))))

(defun finger-map (finger finger-history)
  "Funkcja wywołuje speed-score. Jeśli palec był ostatnio użyty, to odniesieniem jest ostatnie
położenie palca. W przeciwnym wypadku jest to położenie domyślne."
  (if finger-history                    ; jeżeli palec był poprzednio użyty
      (speed-score                      ; wywołj funkcję oceny
       (finger-path-to-vector finger-history finger) ; dla wektora między pozycjami palca
       :lately-used T)                               ; i podaj informację, że był ostatnio użyty
      (speed-score                                   ; w przeciwnym wypadku wywołaj funkcję
       (finger-path-to-vector                        ; dla wektora między pozycją palca i jego
        (cdr (gethash (getf finger :finger)          ; pozycją bazową, zostawiają parametr
                      *fingers*))                    ; :lately-used jako nil (false)
        finger))))

(defun speed-function (current-fingers)
  "Obowiązkowy symbol zdefiniowany w pakiecie"
  (let ((previous-fingers               ; Niech "poprzednie palce"
         (adjust current-fingers        ; będzie wyrównaną tablicą wziętą z historii
                 history                ; w przypadku braku odpowiadającego palca
                 :test (lambda (x y)    ; funkcja przyjmie nil
                         (eql (getf x :finger)
                              (getf y :finger))))))
    (prog1                              ; Wybierz najwyższy (najgorszy wynik)
        (apply #'max
               (mapcar #'finger-map
                       current-fingers
                       previous-fingers))
      (setf history current-fingers)))) ; zapisz obecne palce jako historię

