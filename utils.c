#include "initparse.h"
#include <stdio.h>
#include <stdlib.h>

typedef int (*f_ptr)(Flist **);

f_ptr callback = NULL;

int test(int x) {
  printf("Hello world %d\n", x);
  return x+1;
}

void flush() {
  fflush(stdout);
}

Flist **flist_example() {
  int i=5,j;
  Flist **ptr = malloc( (i+1) * sizeof(Flist *));
  ptr[i] = NULL;
  for(j=0; j<i; j++) {
    ptr[j] = malloc(sizeof(Flist));
    ptr[j]->finger = j;
    ptr[j]->coordinates = NULL;
  }

  return ptr;
}

int reg_callback(f_ptr f) {
  callback = f;
  return 0;
}

int run_callback() {
  Flist **ptr = flist_example();
  return callback(ptr);
}

void test_string(char *s) {
  printf("%s\n", s);
}
