
(in-package :dead-corpse)

(defparameter *libs-dir* 
  (merge-pathnames-as-directory *workspace* #P"libs/")
  "Ścieżka do katalogu ze źródłami")

(define-foreign-library
    (libparse :search-path *libs-dir*)
  (t (:default "libparse")))
(define-foreign-library
    (libstat :search-path *libs-dir*)
  (t (:default "libstat")))
(define-foreign-library
    (utils :search-path *libs-dir*)
  (t (:default "utils")))

(mapcar #'load-foreign-library
	'(libparse libstat utils))

(use-foreign-library libparse)
(use-foreign-library libstat)
(use-foreign-library utils)

(defcfun "register_callbacks" :int
    (speed-funtion :pointer)
    (comfort-function :pointer))

(defcfun "init_hash" :int
  (char-hashtable-size :unsigned-int)
  (keys-hashtable-size :unsigned-int))
(defcfun "free_hash" :void)

(defcfun "reg_key" :int
  (number :unsigned-int)
  (coord :pointer))
(defcfun "reg_char" :int
  (char :unsigned-int)
  (keys :pointer)
  (fingers :pointer))

(defcfun "parse_character" :int
  (char_code :unsigned-int))
;; (defcfun "parse_keys" :int
;;   (tab :pointer))

(defcfun "init_stats" :int
  (agreguj :unsigned-int)
  (zestaw  :string))
(defcfun "finalize_stats" :int)
(defcfun "static_stats_char" :int
  (char_code :unsigned-int))
;; (defcfun "dynamic_stats_fingers" :int
;;   (fingers :pointer))
;; (defcfun "dynamic_stats_speed" :int
;;   (time :int))
;; (defcfun "dynamic_stats_comfort" :int
;;   (comfort :int))

(defcfun "test_string" :void
  (s :string))
(defcfun "flush" :void)
(defcfun "test" :int
  (x :int))
(defcfun "flist_example" :pointer)
