(in-package :dead-corpse)

(defun comfort-score (finger-pos-1 finger-pos-2)
  "Oblicza odleglosc manhattańska pomiedzy dwoma pozycjami palca"
  (apply                                ; wywolaj funkcje
   (lambda (x y)                        ; ktora liczy odleglosc na siatce
     (+ (abs x)
        (abs y)))
   (mapcar                              ; dla listy argumentow
    #'-                                 ; bedacych roznica
    (getf finger-pos-1 :coords)              ; wspolrzednych x oraz y
    (getf finger-pos-2 :coords))))

(defun comfort-function (fingers)
  "Wybiera najwyzsza (najgorsza) z ocen ruchow poszczegolnych palcow"
  (let ((fingers-defaults                ; zadeklaruj zmienna
	 (reverse                        ; zawierajaca liste palcow
	  (mapcar                        ; na ich domyslnych pozycjach
	   (lambda (finger)              
	     "Wyszukuje w globalnej tablicy domyslna pozycje palca finger"
	     (cdr
	      (gethash
	       (getf finger :finger)
	       *fingers*)))
	   fingers))))
    (apply #'max                        ; wybierz maksymalna wartosc
           (mapcar                      ; z listy ocen dla poszczegolnych palcow
            #'comfort-score
            fingers
            fingers-defaults))))
