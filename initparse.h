#ifndef __INITPARSE_H
#define __INITPARSE_H

/* definicja struktury flist */
typedef struct {
	unsigned int finger;
	unsigned int *coordinates;
} Flist;

/* rejestracja callbacków od lispa */
typedef int (*f_ptr)(Flist **);
extern int register_callbacks(f_ptr speed, f_ptr comfort);

/***********      alokacja i zwolnienie pamieci dla potrzebnych struktur         ***************/
extern int init_hash(unsigned int hashsize, unsigned int max_num_of_registered_keys); //zwraca -1 w przypadku niepoprawnej alokacji pamieci
extern void free_hash(void);
/***********************************************************************************************/

/*********** struktura bedaca jednym "polem" tablicy hashowej zawierajacej dane o klawiszach *******************/
struct HashKey {
	unsigned int *coordinates;  		//wspolrzedne danego klawisza
};
/***************************************************************************************************************/


/*********** rejestrowanie klawisza oraz dostep do zarejestrowanego klawisza **********************/
extern signed int reg_key(unsigned int number, unsigned int *coord);
extern struct HashKey* get_key(unsigned int numberOfKey);	//zwraca NULL, gdy klawisz nie zostal zarejestrowany
/**************************************************************************************************/

/************ struktura bedaca jednym "polem" tablicy hashowej zawierajacej dane o znakach *****************/
struct HashChar {
    unsigned int charCode;	   			//kod znaku
	unsigned int* keys;		   			//wektor numerow klawiszy, ktore nalezy wcisnac, aby wprowadzic za pomoca testowanej klawiatury zadany znak
	unsigned int *fingers;		   		//numer palca, ktory wciska dany klawisz (1-10)
    struct HashChar *next;				//wskaznik na nastepny element na liscie, potrzebny do rozwiazywania konfliktow (gdy go nie ma = NULL)
};
/***********************************************************************************************************/

/*********** rejestrowanie znaku oraz dostep do zarejestrowanego znaku **********************/
extern signed int reg_char(unsigned int char_code, unsigned int *keys, unsigned int *fingers);
extern struct HashChar* get_char(unsigned int charCode); //zwraca NULL gdy znak nie zostal zarejestrowany
/**************************************************************************************************/
unsigned int hashCharcode(unsigned int code);	//niewidoczna na zewnatrz funkcja obliczajaca hash dla zadanego kodu unicode
/*unsigned int* getCenterXY();*/


extern signed int parse_character(unsigned int char_code);
extern signed int parse_keys(unsigned int *keys, unsigned int *fingers);
#endif
