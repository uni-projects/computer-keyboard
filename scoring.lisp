(in-package :dead-corpse)

;; (define-foreign-library
;;     (utils :search-path #P"libs/utils.so")
;;   (t (:default "utils")))
;; (load-foreign-library 'utils)
;; (use-foreign-library utils)
;; (defcfun "flist_example" :pointer)
;; (defcfun "reg_callback" :int (callback :pointer))
;; (defcfun "run_callback" :int)

;; callback glue
(defcstruct flist
    (finger :unsigned-int)
    (coords :pointer))

(defun copy-finger (ptr)
  (with-foreign-slots ((finger coords) ptr (:struct flist))
    (list
     :finger (gethash finger *reverse-lookup-fingers*)
     :coords
     (when (not (null-pointer-p coords))
	(do* ((i 0 (1+ i))
	      (c (mem-aref coords :unsigned-int i)
		 (mem-aref coords :unsigned-int i))
	      result)
	     ((= c 0) result)
	  (push c result))))))

(defun copy-flist (fl)
  (do* ((i 0 (1+ i))
	(ptr (mem-aref fl '(:pointer (:struct flist)) i)
	     (mem-aref fl '(:pointer (:struct flist)) i))
	result)
       ((null-pointer-p ptr)
	(reverse result))
    (push (copy-finger ptr) result)))

;; Utilities
(defun adjust (reference sequence &key (test #'eql))
  "Adjusts order of sequence to order of reference. If element doesn't exist,
field is set to NIL. Additional elements are ommited."
  (when reference
    (cons
     (find (car reference)
	   sequence
	   :test test)
     (adjust (cdr reference)
	     (remove (car reference)
		     sequence
		     :test test)
	     :test test))))

(defmacro defwrapper (name target &optional desc)
  "Registers callback which copies flist and calls target function with that copy"
  `(defcallback ,name :int ((fl :pointer))
     (let ((score (,target (copy-flist fl))))
       ;; (format t "~A score is ~A~%" ,desc score)
       (if (numberp score)
	   (floor score)
	   0))))

(load "speed-function.lisp")
(defwrapper
    speed-function-wrapper
    speed-function
  "SPEED")

(load "comfort-function.lisp")
(defwrapper
    comfort-function-wrapper
    comfort-function
  "COMFORT")
