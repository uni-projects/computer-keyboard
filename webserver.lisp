
(ql:quickload '(:swank :hunchentoot :cl-who :cl-fad))
(load "main.lisp")

(defpackage :dead-corpse-webserver
  (:use :cl :hunchentoot :cl-who :cl-fad :dead-corpse))
(in-package :dead-corpse-webserver)

(defvar *httpd-port* 8080)
(defvar *shutdown-port* 6200)
(defvar *swank-port* 4005)

(defvar *path*
  (pathname-directory-pathname *load-pathname*)
  "Ścieżka do katalogu ze źródłami")
(defvar *httpd-/*
  (merge-pathnames-as-directory *path* "web/"))
(defvar *httpd-/configs/*
  (merge-pathnames-as-directory *httpd-/* "configs/"))
(defvar *httpd-/corpses/*
  (merge-pathnames-as-directory *httpd-/* "corpses/"))
(defvar *httpd-/functions/*
  (merge-pathnames-as-directory *httpd-/* "functions/"))
(defvar *httpd-/results/*
  (merge-pathnames-as-directory *httpd-/* "gnuplot/out/"))

(defvar *httpd*
  (hunchentoot:start
   (make-instance 'hunchentoot:easy-acceptor
		  :port *httpd-port*
		  :document-root *httpd-/*)))

(defvar calc-thread nil)

(setf (html-mode) :html5)

(defmacro standard-page (&body body)
  "Makro zawiera szablon strony"
  `(with-html-output-to-string
       (*standard-output* nil :prologue t :indent t)
     (:html
      (:head (:meta :charset "utf-8")
	     (:title "Computer keyboard from future")
	     (:link :type "text/css"
		    :rel "stylesheet"
		    :href "/css/base-v2.css"))
      (:body
       (:header
	:style
	(if (and
	     (bordeaux-threads:threadp calc-thread)
	     (bordeaux-threads:thread-alive-p calc-thread))
	    "background: #ff9494"
	    "")
	(:h1 :class "site-title"
	     "keyfu")
	(:nav
	 (:ul :class "menu"
	      (mapcar (lambda (c x y)
			(htm
			 (:li :class c
			      (:a :href x
				  (fmt "~a" y)))))
		      '("start" "creator-item"
			"configuration-item" "calculation-item"
			"results-item" "authors-item")
		      '("index.html" "creator.html" "dcorpse.html"
			"obliczenia.html" "wyniki.html"
			"autorzy.html")
		      '("Start" "Kreator" "Konfiguracja"
			"Obliczenia" "Wyniki" "Autorzy")))))
       (:div :id "wrapper"
	     ,@body)
       (:script :type "text/javascript"
		:src "js/base.js")
       (:footer
	(:img :src "/img/made-with-lisp.png" :height 75))))))

(defmacro with-open-files (filespecs &body body)
  (if filespecs
      `(with-open-file ,(car filespecs)
	 (with-open-files ,(cdr filespecs)
	   ,@body))
      `(progn ,@body)))

(defmacro config-box ((title submit action) &body body)
  `(htm
    (:div :class "conf"
	  (:form :action ,action
		 :enctype "multipart/form-data"
		 :method "post"
		 (:table :class "fast-start"
			 (:tr
			  (:td
			   (:h3 ,title))
			  (:td
			   (:input :type "submit"
				   :class "btn"
				   :value ,submit)))
			 ,@body)))))

;; (defmacro define-file-saver (name &body body &key uri (&key files names extra))
;;   `(define-easy-handler (,name :uri ,uri)
;;        (,@files
;; 	,@names
;; 	,@extra)
;;      (if (and ,@files ,@names ,@extra)
;; 	 (with-open-files
;; 	     ,(dolist (x files)

(define-easy-handler (index-red :uri "/") ()
    (redirect "/index.html"))

(define-easy-handler (index :uri "/index.html")
    ()
  (standard-page
    (:h1 "Podręcznik użytkownika")
    (mapcar
     (lambda (x)
       (htm (:p (:li (fmt "~a" x)))))
     '("Kreator pozwoli Ci stworzyć wymarzoną klawiaturę w intuicyjny sposób, a także zachować postępy prac w celu późniejszego odtworzenia."
       "Konfiguracja aplikacji znajduje się w zakładce o tejże nazwie. Możliwe jest zapisanie w niej definicji układów klawiatur, korpusów tekstowych i własnych funkcji oceny"
       "Moduł obliczeń pozwoli Ci wybrać konfiguracje, które chciałbyś przetestować. Po rozpoczęciu obliczeń bądź cierpliwy, gdyż mogą zająć trochę czasu. Nie można przeprowadzać dwóch obliczeń w tym samym czasie - to że obliczenia są w toku, sygnalizuje czerwony górny panel. W przeciwnym wypadku panel pozostaje niebieski."
       "Zakładka z wynikami umożliwia obejrzenie różnych ich zestawów. W przypadku większych danych statystycznych ładowanie wykresów może zająć chwilę, gdyż są one interaktywne."
       "W przypadku chęciu poznania autorów aplikacji, możesz użytkowniku wejść na ostatnią zakładkę."))))

(define-easy-handler (la-la-la*muahahahaha :uri "/go")
    ((par1 :request-type :post)
     (par2 :request-type :post)
     (par3 :request-type :post)
     (par4 :request-type :post)
     (par5 :request-type :post))
  (when (and par1 par2 par3 par4 par5)
    (unless (and
	     (bordeaux-threads:threadp calc-thread)
	     (bordeaux-threads:thread-alive-p calc-thread))
      (setf *aggregation* (parse-integer par5 :junk-allowed 0))
      (setf calc-thread
	    (bordeaux-threads:make-thread
	     (lambda ()
	       (calculate par1 par2 par3 par4)))))
    (redirect "/obliczenia.html")))



(defun calculate (par1 par2 par3 par4)
  (when (and par1 par2 par3 par4)
    (load
     (merge-pathnames *httpd-/functions/* par4))
    (initialize
     (merge-pathnames *httpd-/configs/* par2))
    (parse-corpus
     (merge-pathnames *httpd-/corpses/* par3)
     :path (concatenate 'string
			par1 "/"))
    (finalize)))

;  (redirect "/obliczenia.html"))

(define-easy-handler (add-keyboard :uri "/add-keyboard")
    ((map1 :request-type :post)
     (map2 :request-type :post)
     (map3 :request-type :post)
     (par1 :request-type :post))
     ;; (par2 :request-type :post))
  (if (and par1 #|par2|# map1 map2 map3)
      (with-open-files
	  ((stream1 (car map1))
	   (stream2 (car map2))
	   (stream3 (car map3))
	   (stream4 (merge-pathnames *httpd-/configs/* par1)
		    :direction :output
		    :if-exists :supersede))
	(format stream4 "(in-package :dead-corpse)~%")
	(do ((redirected-stream
	      (make-echo-stream
	       (make-concatenated-stream stream1 stream2 stream3)
	       stream4)))
	    ((not (read-line redirected-stream
			     nil nil)) T))))
  (redirect "/dcorpse.html"))

(define-easy-handler (add-corpus :uri "/add-corpus")
    ((par1 :request-type :post)
     (corp :request-type :post))
  (if (and par1 corp)
      (with-open-files
	  ((stream1 (car corp))
	   (stream2 (merge-pathnames *httpd-/corpses/* par1)
		    :direction :output
		    :if-exists :supersede))
	(do ((redirected-stream
	      (make-echo-stream
	       (make-concatenated-stream stream1)
	       stream2)))
	    ((not (read-line redirected-stream
			     nil nil)) T))))
  (redirect "/dcorpse.html"))

(define-easy-handler (add-functions :uri "/add-functions")
    ((par1 :request-type :post)
     (fun1 :request-type :post)
     (fun2 :request-type :post))
  (if (and par1 fun1 fun2)
      (with-open-files
	  ((stream1 (car fun1))
	   (stream2 (car fun2))
	   (stream3 (merge-pathnames *httpd-/functions/* par1)
		    :direction :output
		    :if-exists :supersede))
	(do ((redirected-stream
	      (make-echo-stream
	       (make-concatenated-stream stream1 stream2)
	       stream3)))
	    ((not (read-line redirected-stream
			     nil nil)) T))))
  (redirect "/dcorpse.html"))

(define-easy-handler (dcorpse :uri "/dcorpse.html") ()
  (standard-page
    (:h1 "Konfiguracja aplikacji")
    (config-box
	("Dodaj Konfigurację Klawiatury"
	 "Dodaj klawiaturę"
	 "/add-keyboard")
      (:tr
       (:td "Nazwa konfiguracji klawiatury")
       (:td
	(:input :type :text
		:name "par1")))
      (mapcar (lambda (x y z)
		(htm
		 (:tr (:td (fmt x))
		      (:td (:input
			    :type y
			    :name z)))))
	      '("Mapa palców"
		"Mapa klawiszy"
		"Mapa znaków")
	      '("file" "file" "file")
	      '("map1" "map2" "map3")))
    (config-box 
	("Dodaj korpus referencyjny"
	 "Dodaj korpus"
	 "/add-corpus")
      (:tr
       (:td "Nazwa tekstu źródłowego")
       (:td
	(:input :type :text
		:name "par1")))
      (:tr
       (:td "Ścieżka do pliku z tekstem")
       (:td (:input :type "file"
		    :name "corp"))))
    (config-box ("Dodaj funkcje oceny"
		 "Dodaj funkcje"
		 "/add-functions")
      (:tr
       (:td "Nazwa zestawu funkcji")
       (:td
	(:input :type :text
		:name "par1")))
      (:tr
       (:td "Funkcja oceny szybkości")
       (:td (:input :type "file"
		    :name "fun1")))
      (:tr
       (:td "Funkcja oceny komfortu")
       (:td (:input :type "file"
		    :name "fun2"))))))


(define-easy-handler (obliczenia :uri "/obliczenia.html")
    ()
  (standard-page
    (:h1 "Przeprowadź obliczenia")
    (config-box ("Przeprowadź obliczenia" "Rozpocznij" "/go")
      (:tr
       (:td "Nazwa eksperymentu")
       (:td
	(:input :type :text
		:name "par1")))
      (:tr
       (:td "Konfiguracja klawiatury")
       (:td
	(:select :name "par2"
		 (mapcar (lambda (x)
			   (htm (:option (fmt "~a" (pathname-name x)))))
			 (list-directory "web/configs/")))))
      (:tr
       (:td "Konfiguracja korpusu tekstowego")
       (:td
	(:select :name "par3"
		 (mapcar (lambda (x)
			   (htm (:option (fmt "~a" (pathname-name x)))))
			 (list-directory "web/corpses/")))))
      (:tr
       (:td "Konfiguracja z funkcjami oceny")
       (:td
	(:select :name "par4"
		 (mapcar (lambda (x)
			   (htm (:option (fmt "~a" (pathname-name x)))))
			 (list-directory "web/functions/")))))
      (:tr
       (:td "Szerokość przedziału agregowania")
       (:td (:input :type :number
		    :value 50
		    :name "par5"))))
    (config-box ("Ustaw wyniki do pokazania" "Pokaż wyniki" "/wyniki.html")
      (:tr
       (:td "Wybierz eksperyment")
       (:td
	(:select :name "konfiguracja"
		 (mapcar (lambda (x)
			   (htm (:option 
				 (fmt "~a"
				      (car
				       (last
					(pathname-directory x)))))))
			 (remove-if-not
			  #'directory-pathname-p
			  (list-directory "web/gnuplot/out//")))))))))
      

(define-easy-handler (wyniki :uri "/wyniki.html")
    (konfiguracja)
  (if konfiguracja
      (setf *dispatch-table*
	    (cons
	     (create-folder-dispatcher-and-handler
	      "/gnuplot/out/"
	      (merge-pathnames-as-directory
	       *httpd-/results/*
	       (concatenate 'string
			    konfiguracja
			    "/")))
	     (last *dispatch-table*))))
  (handle-static-file
   (merge-pathnames-as-file *httpd-/* "wyniki.html")))

(define-easy-handler (autorzy :uri "/autorzy.html")
    ()
  (standard-page
    (:h1 "Autorzy aplikacji KeyFu")
    (:table
     (mapcar (lambda (x y z)
	       (htm
		(:tr (:td :class "avatar"
			  (:img :src x :height 100))
		     (:td :class "description"
			  (:h3 (fmt "~a" y))
			  (if (listp z)
			      (dolist (v z)
				(htm (:li (fmt "~a" v))))
			      (htm (:p (fmt "~a" z))))))))
	     '("/img/daniel-kochmanski.svg"
	       "/img/michal-nowacki.svg"
	       "/img/marcin-nozewski.svg"
	       "/img/wojciech-zamozniewicz.svg"
	       "/img/maciej-milostan.svg"
	       "/img/grzegorz-pawlak.svg")
	     '("Daniel Kochmański"
	       "Michał Nowacki"
	       "Marcin Nożewski"
	       "Wojciech Zamożniewicz"
	       "dr inż. Maciej Miłostan"
	       "dr inż. Grzegorz Pawlak")
	     '(("Implementacja referencyjnych funkcji oceny"
		"Integracja modułów")
	       ("Zarządzanie pamięcią aplikacji"
		"Kontrola przepływu danych")
	       ("Opracowanie i stworzenie modułu obliczającego statystyki"
		"Prezentacja statystyk")
	       ("Stworzenie interfejsu użytkownika"
		"Napisanie narzędzia do tworzenia klawiatur")
	       ("Opiekun grupy"
		"Wsparcie merytoryczne")
	       ("Promotor grupy"
		"Wsparcie merytoryczne"))))))
