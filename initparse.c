#include <stdio.h>
#include <stdlib.h>
#include "initparse.h"
#include "stat.h"

f_ptr speed_function = NULL,
  comfort_function = NULL;

int register_callbacks(f_ptr speed, f_ptr comfort) {
  speed_function = speed;
  comfort_function = comfort;
  return 0;
}


static unsigned int HASHSIZE = 0;
static unsigned int MAX_NUM_OF_KEYS = 0;

/************************************************************/
/*************************************************************

				TABLICA HASHOWA DLA KLAWISZY

*************************************************************/
/************************************************************/
static struct HashKey **keysHashTab = NULL;

struct HashKey* get_key(unsigned int);


/**** Funkcja rejestrująca klawisz i jego wspolrzedne ****/
signed int reg_key(unsigned int number, unsigned int *coord)
{
	#ifdef DEBUG
		printf("reg_key: number=%d coord={ ", number);
		unsigned int *c = coord;
		if(!c) {
			printf("NULL POINTER ");
		} else {
			while(*c != 0) {
				printf("%d ", *c);
				c++;
			}
		}
		printf("} ");
	#endif

	if(!coord) {
		#ifdef DEBUG
			printf("-2 returned! (empty coord-vec)\n");
		#endif
		return -2;
	}
	else if(coord[0]<=0 || coord[1]<=0 || coord[2]!=0)
	{
		#ifdef DEBUG
			printf("-3 returned! (0 value passed as coord-vec)\n");
		#endif
		return -3;
	}
	
	if(get_key(number)!=NULL)
	{
		#ifdef DEBUG
			printf("-1 returned! (The key already registered)\n");
		#endif
		return -1;
	}

	keysHashTab[number] = (struct HashKey*)malloc(sizeof(struct HashKey));
	keysHashTab[number]->coordinates = coord;
	
	#ifdef DEBUG
		printf("SUCCESS!\n");
	#endif
	return 0;
}


/**** Funkcja wyciaga wspolrzedne danego klawisza ****/
struct HashKey* get_key(unsigned int number)
{
#ifdef DEBUG
	if(keysHashTab[number]!=NULL)
		printf("get_key: Znaleziono klawisz o numerze %d\n", number);
	else
		printf("get_key: Nie znaleziono klawisza o numerze %d\n", number);
#endif

	if(number>=MAX_NUM_OF_KEYS) 
		return NULL;
	else
		return keysHashTab[number];
}
/************************************************************/
/************************************************************/





/************************************************************/
/*************************************************************

				TABLICA HASHOWA DLA ZNAKÓW

*************************************************************/
/************************************************************/
static struct HashChar **charHashTab = NULL;

unsigned int hashCharcode(unsigned int code)
{
	unsigned int modifier1, m2, m3, m4, m5, m6, m7;
	
		modifier1= code%17389;
		m2= code%13;
		m3= code/29;
		m4= code%60527;
		m5= code%63727;
		m6= code%27449;
		m7= code%81799;
	
	#ifdef DEBUG
		printf("hashCharcode: code=%d -> hash=%d\n", code, ((modifier1+m2+m3+m4+m5+m6+m7+code)%HASHSIZE));
	#endif
		
	return ((modifier1+m2+m3+m4+m5+m6+m7+code)%HASHSIZE);
}

// Funkcja rejestruje kombinację klawiszy konieczną do uzyskania znaku char_code
signed int reg_char(unsigned int char_code, unsigned int *keysVec, unsigned int *fingersVec)
{

	#ifdef DEBUG
		printf("reg_char: char_code=%d keys={ ", char_code);
		unsigned int *k1 = keysVec;
		
		if(!k1) {
			printf("NULL POINTER");
		} 
		else {
			while(*k1 != 0) {
				printf("%d ", *k1);
				k1++;
			}
		}	
		printf("} fingers={");
		
		unsigned int *k2 = fingersVec;
		if(!k2) {
			printf("NULL POINTER");
		} 
		else {
			while(*k2 != 0) {
				printf("%d ", *k2);
				k2++;
			}
		}
		printf("}\n");
	#endif

	if(!keysVec) {
		#ifdef DEBUG
			printf("-2 returned! (empty keys-vec)\n");
		#endif
		return -2;
	}
  
	if(get_char(char_code)!=NULL)
	{
		#ifdef DEBUG
			printf("-1 returned! (char already registered)\n");
		#endif
		return -1;
	}
  
	unsigned int hash= hashCharcode(char_code);
	
	if(charHashTab[hash]==NULL)
	{
		charHashTab[hash] = (struct HashChar*)malloc(sizeof(struct HashChar));
		charHashTab[hash]->next = NULL;
		charHashTab[hash]->charCode = char_code;
		charHashTab[hash]->keys = keysVec;
		charHashTab[hash]->fingers = fingersVec;
	}
	else
	{
		struct HashChar *tempNext = charHashTab[hash]->next;
		struct HashChar *tempPrev = charHashTab[hash];
		
		if(charHashTab[hash]->charCode > char_code)
		{
			charHashTab[hash] = (struct HashChar*)malloc(sizeof(struct HashChar));
			charHashTab[hash]->next = tempPrev;
			charHashTab[hash]->charCode = char_code;
			charHashTab[hash]->keys = keysVec;
			charHashTab[hash]->fingers = fingersVec;
		}
		else
		{
			if(tempNext==NULL)
			{
					tempNext = (struct HashChar*)malloc(sizeof(struct HashChar));
					tempPrev->next = tempNext;
					tempNext->next = NULL;
					tempNext->charCode = char_code;
					tempNext->keys = keysVec;
					tempNext->fingers = fingersVec;
					#ifdef DEBUG
						printf("SUCCESS!\n");
					#endif
					return 0;
			}
			else
			{
				while(tempNext->charCode < char_code)
				{
					if(tempNext->next==NULL)
					{
						tempNext->next = (struct HashChar*)malloc(sizeof(struct HashChar));
						tempNext->next->next = NULL;
						tempNext->next->charCode = char_code;
						tempNext->next->keys = keysVec;
						tempNext->next->fingers = fingersVec;
						#ifdef DEBUG
							printf("SUCCESS!\n");
						#endif
						return 0;
					}
					
					tempNext = tempNext->next;
					tempPrev = tempPrev->next;
				}
				
				tempPrev->next = (struct HashChar*)malloc(sizeof(struct HashChar));
				tempPrev->next->next = tempNext;
				tempPrev->next->charCode = char_code;
				tempPrev->next->keys = keysVec;
				tempPrev->next->fingers = fingersVec;
				
			}		
		}		
	}
	
	#ifdef DEBUG
		printf("SUCCESS!\n");
	#endif
	return 0;
}

/**** Funkcja wyciaga dane z tablicy hashowej dot. znakow ****/
struct HashChar* get_char(unsigned int charCode)
//zwraca NULL, jezeli znaku nie ma w tablicy hashowej
{
	unsigned int calculatedHash = hashCharcode(charCode);
	struct HashChar *tempCurrent= charHashTab[calculatedHash]; 
	
	if(charHashTab[calculatedHash] != NULL)
		while(tempCurrent->charCode <= charCode)
		{
			if(tempCurrent->charCode == charCode)
			{
				#ifdef DEBUG
					printf("get_char: Znaleziono znak o kodzie %d\n", charCode);
				#endif
				return tempCurrent;
			}
				
			tempCurrent = tempCurrent->next;
			
			if(tempCurrent == NULL)
				break;
		}
	
	#ifdef DEBUG
		printf("get_char: Nie znaleziono znaku o kodzie %d\n", charCode);
	#endif
	return NULL;
}
/************************************************************/
/************************************************************/



/************************************************************/
/************************************************************/
int init_hash(unsigned int hashsize, unsigned int max_num_of_registered_keys)
{
	HASHSIZE = hashsize;
	MAX_NUM_OF_KEYS = max_num_of_registered_keys;
	keysHashTab = (struct HashKey**)malloc(MAX_NUM_OF_KEYS*sizeof(struct HashKey*));
	charHashTab = (struct HashChar**)malloc(HASHSIZE*sizeof(struct HashChar*));
	
	if(keysHashTab == NULL || charHashTab == NULL)
	{
		#ifdef DEBUG
			printf("init_hash: Błąd przy inicjalizacji tablic!\n");
		#endif
		return -1;
	}
	
	unsigned int u;
	for(u=0; u<HASHSIZE; u++)
	{
		charHashTab[u]=NULL;
	}
	for(u=0; u<MAX_NUM_OF_KEYS; u++)
	{
		keysHashTab[u]=NULL;
	}
	
	#ifdef DEBUG
		printf("init_hash: Inicjalizacja tablic zakończona sukcesem!\n");
	#endif
	return 0;
}


void free_hash(void)
{
	struct HashChar *tempNext = NULL;
	struct HashChar *tempPrev = NULL;
	
	unsigned int i=0;
	for(i=0; i<HASHSIZE; i++)
	{
		if(charHashTab[i] == NULL) continue;
	
		if(charHashTab[i]->next == NULL)
		{
			free(charHashTab[i]->keys);
			free(charHashTab[i]->fingers);
			free(charHashTab[i]);	//zwolnienie bezkolizyjnych struktur HashChar
		} 
		else
		{
			tempNext = charHashTab[i]->next;
			tempPrev = charHashTab[i];
			while(tempNext != NULL)	//sekw. zwolnienie kolizyjnych struktur HashChar
			{
				free(tempPrev->keys);
				free(tempPrev->fingers);
				free(tempPrev);
				tempPrev = tempNext;
				tempNext = tempNext->next;				
			}
			
			free(tempPrev->keys);
			free(tempPrev->fingers);
			free(tempPrev);
		}
	}
	free(charHashTab); //zwolnienie miejsca zajmowanego przez wskazniki do struktur HashChar
	
	
	for(i=0; i<MAX_NUM_OF_KEYS; i++)
	{
	  if(keysHashTab[i]) {
		free(keysHashTab[i]->coordinates);	//zwalniam pamiec zajmowana przez wektory wspolrzednych
		free(keysHashTab[i]);	//wszystkie struktury HashKey sa bezkolizyjne, wiec zwalniamy normalnie
	  }
	}
	free(keysHashTab); //zwolnienie miejsca zajmowanego przez wskazniki do struktur HashKey
	
	#ifdef DEBUG
		printf("free_hash: Zwalnianie pamięci zakończone.\n");
	#endif
}

/************************************************************/
/************************************************************/
/************************************************************/


// Funkcja przyjmuje kod liczbowy znaku i odwzorowuje go na klawisze, które
// trzeba wcisnąć, aby go uzyskać. Następnie wywołuje funkcję parse_keys.
signed int parse_character(unsigned int char_code) {
	if(get_char(char_code)==NULL)
	{
		#ifdef DEBUG
			printf("parse_character: char_code=%d funkcja zwraca -1\n(podany znak nie zostal zarejestrowany)\n", char_code);
		#endif
		return -1;
	}

	struct HashChar* hashPos = get_char(char_code);
	
	parse_keys(hashPos->keys, hashPos->fingers);
	
	#ifdef DEBUG
		printf("parse_character: char_code=%d funkcja zwraca 0\n", char_code);
	#endif
	return 0;
}


/* Funkcja przyjmuje tablicę klawiszy koniecznych do uzyskania znaku
 * podanego do funkcji parse_character i przetwarza je na ich wspolrzedne.
 * Przyjmuje też tablice z palcami uzytymi do wcisniecie kombinacji klawiszy. 
 * Potem następuje wywołanie funkcji oceny oraz statystycznych.  */
signed int parse_keys(unsigned int* keys, unsigned int* fingers) {

	if(keys == NULL || keys[0] == 0) {
		#ifdef DEBUG
			printf("parse_keys: empty keys-vec, -2 returned\n");
		#endif
		return -2;
	}
	if(fingers == NULL || fingers[0] == 0)
	{	
		#ifdef DEBUG
			printf("parse_keys: empty fingers-vec, -2 returned\n");
		#endif
		return -2;
	}
	
	#ifdef DEBUG
		printf("parse_keys: keys={ ");
		unsigned int *k = keys;

		while(*k != 0) {
			printf("%d ", *k);
			k++;
		}
		printf("} ");
	#endif

	unsigned int i, size = 0;
	for(i=0; keys[i]>0; i++)
	{
		++size;
	}

	dynamic_stats_fingers(fingers);
	
	Flist **list = (Flist**)malloc((size+1)*sizeof(Flist*));
	
	
	for(i=0; i<size; i++)
	{
		list[i]= (Flist*)malloc(sizeof(Flist));
		list[i]->finger = fingers[i];
		list[i]->coordinates = get_key(keys[i])->coordinates;
	}
	list[size] = NULL;
	
	dynamic_stats_speed( speed_function(list) );
	dynamic_stats_comfort( comfort_function(list) );

	for(i=0; i<size;i++) {
	  free(list[i]);
	}
	free(list);
  
 
	#ifdef DEBUG
		printf("parse_keys: SUCCESS!\n");
	#endif
	return 0;
}
