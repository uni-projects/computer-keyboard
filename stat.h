#ifndef _STAT_H_
#define _STAT_H_

extern int static_stats_char (unsigned int char_code);
extern int dynamic_stats_fingers (unsigned int *fingers);
extern int dynamic_stats_speed (int time);
extern int dynamic_stats_comfort (int com);
extern int init_stats(unsigned int agreguj, char *sciezka);
extern int finalize_stats();
extern int get_stats_char();
extern int get_fingers_total(); 

#endif
